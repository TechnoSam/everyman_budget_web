function gebi(id) {
    return document.getElementById(id);
}

// Set the total balance
setTotalBalanceCard();

var currencyFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits: 4,
});

async function setTotalBalanceCard() {
    var totalBalance = await getTotalBalance();
    gebi('total-balance-card-body').textContent = currencyFormatter.format(totalBalance);

}

async function getTotalBalance() {
    var balance = null;
    await fetch('/accounts/balance/', {
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching total balance: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message'] + ' (This is probably a bug)');
                for (var param in data['data']) {
                    if (param === 'date') {
                        setAccountFilterDateError(data['data'][param]['message'] + ' (This is probably a bug)')
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred fetching total balance, see server logs for details');
            }
        }
        else {
            balance = 0;
            var result = data['result'];
            for (var key in result) {
                balance += Number(result[key]['balance']);
            }

        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching total balance, see server logs for details');
    });
    return balance;
}
