function gebi(id) {
    return document.getElementById(id);
}

// Listeners
// Show Add Party Form
gebi('add-party-btn').addEventListener('click', toggleAddPartyFormVisibility);

// Add Party Submit Button
gebi('add-party-submit').addEventListener('click', submitAddParty);

// Clear errors on edit
gebi('add-party-name').addEventListener('change', clearAddPartyNameError);

// Set max-height for animation
var addPartyForm = gebi('add-party-form');
var addPartyFormMaxHeight = addPartyForm.scrollHeight + "px";
addPartyForm.style['overflow'] = 'hidden';
addPartyForm.style['max-height'] = '0px';
addPartyForm.setAttribute('aria-hidden', 'true');

// Initialize Add Party Form
resetAddPartyForm();

// Fetch data
refreshPartyTable();

// Variable to store the table rows that are being edited so they can be replaced after saving or canceling
var editingParties = {};

/**
 * Toggles the Add Party form visibility. If it is hidden, it will slide down into view, if it is
 * visible, it will slide up out of view.
 */
async function toggleAddPartyFormVisibility() {
    if (addPartyForm.style['max-height'] === '0px') { // hidden
        addPartyForm.style['max-height'] = addPartyFormMaxHeight;
        await sleep(300);
        addPartyForm.style['overflow'] = 'visible';
        addPartyForm.setAttribute('aria-hidden', 'false');
    }
    else { // not hidden
        addPartyForm.style['overflow'] = 'hidden';
        addPartyForm.style['max-height'] = '0px';
        await sleep(300);
        addPartyForm.setAttribute('aria-hidden', 'true');
    }
}

/**
 * Submits the data in the Add Party form to the server.
 *
 * If there are any errors, a notification will be created that explains the error. If there is an error with any of the
 * field data, each field will be highlighted and given a tooltip that explains the problem. If the party is submitted
 * successfully, a notification will be created and the table will be refreshed to show the change.
 */
function submitAddParty() {
    var name = gebi('add-party-name').value;

    clearAddPartyFormErrors();

    fetch('/parties/add/', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'name': name})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'name') {
                        gebi('add-party-name').classList.add('icon-input-error');
                        setAddPartyNameTooltip(data['data'][param].message);
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            makeSuccessNotification('Added new party');
            resetAddPartyForm();
            refreshPartyTable();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });
}

/**
 * Clear all errors in the Add Party form
 */
function clearAddPartyFormErrors() {
    clearAddPartyNameError();
}

function clearAddPartyNameError() {
    gebi('add-party-name').classList.remove('icon-input-error');
    clearAddPartyNameTooltip();
}

/**
 * Reset the Add Party form to it's initial state
 */
function resetAddPartyForm() {
    clearAddPartyFormErrors();
    gebi('add-party-name').value = '';
}

/**
 * Set the tooltip for the Add Party name field
 */
function setAddPartyNameTooltip(text) {
    var tt = gebi('add-party-name-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

/**
 * Remove the Add Party name field tooltip
 */
function clearAddPartyNameTooltip() {
    var tt = gebi('add-party-name-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

/**
 * Fetch the parties from the server and populate the table with the data
 */
function refreshPartyTable() {
    fetch('/parties/get/', {
        method: 'GET',
        headers: { 'content-type': 'application/json' }
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching parties: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification('A parameter error occurred fetching parties: ' + data['message']
                + ' (This is likely a bug)');
            }
            else {
                makeErrorNotification('An unknown error occurred fetching parties, see server logs for details');
            }
        }
        else {
            clearPartyTable();
            var parties = data['result']['parties'];
            parties.sort(function(a, b) {return a['name'].localeCompare(b['name'])})
            for (var index = 0; index < parties.length; index++) {
                addPartyToTable(parties[index]);
            }
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching parties, see server logs for details');
    });
}

/**
 * Remove all parties from the table
 */
function clearPartyTable() {
    var tbody = gebi('parties-table-body');
    var newTbody = document.createElement('tbody');
    newTbody.id = 'parties-table-body';
    tbody.replaceWith(newTbody);
}

/**
 * Adds a new party to the bottom of the table.
 *
 * @param {party} party - The party to add.
 */
function addPartyToTable(party) {
    var row = document.createElement('tr');
    var id = party['id'];
    row.id = 'party-' + id;
    var name = party['name'];
    row.innerHTML = `
        <td class="party-data-table-column-1">
            <span id="party-${id}-name">${name}</span>
        </td>
        <td class="party-data-table-column-2">
            <div class="action-btn-group">
                <div id="party-${id}-edit" class="action-btn-group-btn action-btn-group-first action-btn-group-last action-btn-group-blue-1" title='Edit "${name}"'>
                    <i class="icon icon-pencil"></i>
                </div>
            </div>
        </td>
    `
    gebi('parties-table-body').appendChild(row);
    gebi('party-' + party['id'] + '-edit').addEventListener('click', switchToEdit);
}

/**
 * Switches the party's row in the table to "edit" mode.
 *
 * @param {event} event - The event fired from the edit button.
 */
function switchToEdit(event) {
    // party-{id}-edit
    var id = event.currentTarget.id.slice(6, -5);
    var row = gebi('party-' + id);
    var name = gebi('party-' + id + '-name').textContent;
    editingParties[id] = row.cloneNode(true);

    var editRow = document.createElement('tr');
    editRow.id = 'party-' + id;
    editRow.innerHTML = `
        <td class="party-data-table-column-1">
            <div class="icon-input-div edit-party-font-size edit-party-input-font-color tooltip">
                <i class="icon icon-title icon-input-icon"></i>
                <input class="icon-input add-party-input" id="edit-party-${id}-name" type="text" placeholder="Name*" value="${name}">
                <span class="tooltip-text" id="edit-party-${id}-name-tooltip">Default Text</span>
            </div>
        </td>
        <td class="party-data-table-column-2">
            <div class="action-btn-group">
                <div id="party-${id}-save" class="action-btn-group-btn action-btn-group-first action-btn-group-green-3" title="Save">
                    <i class="icon icon-floppy"></i>
                </div>
                <div id="party-${id}-cancel" class="action-btn-group-btn action-btn-group-last action-btn-group-red-1" title="Cancel">
                    <i class="icon icon-cancel"></i>
                </div>
            </div>
        </td>
    `
    row.replaceWith(editRow);

    // Event Listeners
    // Clear errors on name field when changed
    gebi('edit-party-' + id + '-name').addEventListener('change', clearEditPartyNameErrorEvent);
    // Save the new state when the save button is clicked
    gebi('party-' + id + '-save').addEventListener('click', saveEditParty);
    // Cancel the edit when the cancel button is clicked
    gebi('party-' + id + '-cancel').addEventListener('click', cancelEditParty);

    // Initialize
    clearEditPartyNameTooltip(id);
}

/**
 * Clear any Edit Party name field error when the field is changed.
 *
 * @param {Event} event - The change event dispatched from the field.
 */
function clearEditPartyNameErrorEvent(event) {
    // edit-party-{id}-name
    var id = event.currentTarget.id.slice(11, -5);
    clearEditPartyNameError(id);
}

/**
 * Set an Edit Party name field error.
 *
 * Highlights the field and adds the tooltip.
 * @param {string} id - The ID of the party to set.
 * @param {string} text - The error message to show.
 */
function setEditPartyNameError(id, text) {
    gebi('edit-party-' + id + '-name').classList.add('icon-input-error');
    setEditPartyNameTooltip(id, text);
}

/**
 * Clear any Edit Party name field error.
 *
 * @param {string} id - The ID of the party to clear.
 */
function clearEditPartyNameError(id) {
    var elem = gebi('edit-party-' + id + '-name');
    elem.classList.remove('icon-input-error');
    clearEditPartyNameTooltip(id);
}

/**
 * Set the tooltip for an Edit Party name field.
 *
 * @param {string} id - The id of the party to set.
 * @param {string} text - The text to set the tooltip to.
 */
function setEditPartyNameTooltip(id, text) {
    var tt = gebi('edit-party-' + id + '-name-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

/**
 * Clear an Edit Party name field tooltip
 *
 * @param {string} id - The ID of the party to clear
 */
function clearEditPartyNameTooltip(id) {
    var tt = gebi('edit-party-' + id + '-name-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

/**
 * Save the changes to the party by sending the new data to the server and updating the values with the reply.
 * If there are errors, the fields will be highlighted and tooltip the same as when adding a new party.
 *
 * @param {Event} event - The click event from the save button.
 */
function saveEditParty(event) {
    // party-{id}-save
    var id = event.currentTarget.id.slice(6, -5);
    var name = gebi('edit-party-' + id  + '-name').value;

    clearEditPartyErrors(id);

    fetch('/parties/' + id + '/', {
        method: 'PUT',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'name': name})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'name') {
                        setEditPartyNameError(id, data['data'][param].message);
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            row = gebi('party-' + id);
            row.replaceWith(editingParties[id])
            delete editingParties[id];
            gebi('party-' + id + '-name').textContent = data['result']['name'];
            gebi('party-' + id + '-edit').addEventListener('click', switchToEdit);
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });

}

/**
 * Clear all errors in an Edit Party form.
 *
 * @param {string} id - The ID of the party to clear.
 */
function clearEditPartyErrors(id) {
    clearEditPartyNameError(id);
}

/**
 * Cancel editing a party and return the row to it's original state.
 *
 * @param {Event} event - The click event from the cancel button.
 */
function cancelEditParty(event) {
    // party-{id}-cancel
    var id = event.currentTarget.id.slice(6, -7);
    row = gebi('party-' + id);
    row.replaceWith(editingParties[id])
    delete editingParties[id];
    gebi('party-' + id + '-edit').addEventListener('click', switchToEdit);
}
