function gebi(id) {
    return document.getElementById(id);
}

// Listeners
// Show Add Tx Form
gebi('add-tx-btn').addEventListener('click', toggleAddTxVisibility);

// Show Add Transfer Form
gebi('add-transfer-btn').addEventListener('click', toggleAddTransferVisibility);

// Show Account Filters
gebi('show-account-filter-btn').addEventListener('click', toggleAccountFiltersVisibility);

// Show Tx Filters
gebi('show-tx-filter-btn').addEventListener('click', toggleTxFiltersVisibility);

// Submit Add Tx
gebi('add-tx-submit').addEventListener('click', submitAddTx);

// Clear errors on edit
gebi('add-tx-date').addEventListener('change', clearAddTxDateError);
gebi('add-tx-charge').addEventListener('change', clearAddTxChargeError);
gebi('add-tx-party').addEventListener('change', clearAddTxPartyError);
gebi('add-tx-account').addEventListener('change', clearAddTxAccountError);
gebi('add-tx-alt-date').addEventListener('change', clearAddTxAltDateError);
gebi('add-tx-notes').addEventListener('change', clearAddTxNotesError);

// Submit Add Transfer
gebi('add-transfer-submit').addEventListener('click', submitAddTransfer);

// Account Filter Change
gebi('accounts-balance-as-of').addEventListener('change', refreshAccountTable);
// Show Accounts Table
gebi('show-account-table-btn').addEventListener('click', toggleAccountsTableVisibility);

// Tx Filter Change
gebi('tx-filters-accounts-input').addEventListener('change', refreshTxTable);
gebi('tx-filters-parties-input').addEventListener('change', refreshTxTable);
gebi('tx-filters-notes-input').addEventListener('change', refreshTxTable);
gebi('tx-filters-min-charge-input').addEventListener('change', refreshTxTable);
gebi('tx-filters-max-charge-input').addEventListener('change', refreshTxTable);
gebi('tx-filters-date-begin-input').addEventListener('change', refreshTxTable);
gebi('tx-filters-date-end-input').addEventListener('change', refreshTxTable);
// Debit Button
gebi('tx-filters-debit-btn').addEventListener('click', toggleTxFilterDebit);

// Add Tx Credit
gebi('add-tx-credit').addEventListener('click', toggleAddTxCredit);

// Set Main Div height to limit it to one screen height
var mainDiv = gebi('main');
var mainMarginHeight = '(' + window.getComputedStyle(mainDiv).marginTop
                    + ' + ' + window.getComputedStyle(mainDiv).marginBottom + ')';
var navBar = gebi('navbar');
var navBarHeight = window.getComputedStyle(navBar).height;

mainDiv.style.height = 'calc(100vh - ' + mainMarginHeight + ' - ' + navBarHeight + ')';

// Initialize Add Tx Form
// Set max-height for animation
var addTxForm = gebi('add-tx-form');
var addTxFormMaxHeight = addTxForm.scrollHeight + "px";
addTxForm.style['overflow'] = 'hidden';
addTxForm.style['max-height'] = '0px';
addTxForm.setAttribute('aria-hidden', 'true');
// Reset
resetAddTxForm();

// Initialize Add Transfer Form
// Set max-height for animation
var addTransferForm = gebi('add-transfer-form');
var addTransferFormMaxHeight = addTransferForm.scrollHeight + "px";
addTransferForm.style['overflow'] = 'hidden';
addTransferForm.style['max-height'] = '0px';
addTransferForm.setAttribute('aria-hidden', 'true');
// Reset
resetAddTransferForm();

// Initialize Account Filters
// Set max-height for animation
var accountFilters = gebi('account-filters');
var accountFiltersMaxHeight = accountFilters.scrollHeight + "px";
accountFilters.style['overflow'] = 'hidden';
accountFilters.style['max-height'] = '0px';
accountFilters.setAttribute('aria-hidden', 'true');
// Reset
clearAccountFilterErrors();

// Initialize Tx Filters
// Set max-height for animation
var txFilters = gebi('tx-filters');
var txFiltersMaxHeight = txFilters.scrollHeight + "px";
txFilters.style['overflow'] = 'hidden';
txFilters.style['max-height'] = '0px';
txFilters.setAttribute('aria-hidden', 'true');
// Reset
resetTxFilters();

// Setup autocomplete
var accountAutocompleteSuggestions = null;
var partyAutocompleteSuggestions = null;
setupAutocompleteTx();

// Fetch data
refreshAccountTable();
refreshTxTable();

// Variable to store the table rows that are being edited so they can be replaced after saving or canceling
var editingTxs = {};

/**
 * Toggles the Add Tx form visibility. If it is hidden, it will slide down into view, if it is
 * visible, it will slide up out of view.
 */
async function toggleAddTxVisibility() {
    if (addTxForm.style['max-height'] === '0px') { // hidden
        if (addTransferForm.style['max-height'] !== '0px') { // if transfer form is showing, hide it
            toggleAddTransferVisibility();
        }
        addTxForm.style['max-height'] = addTxFormMaxHeight;
        //await sleep(300);
        addTxForm.style['overflow'] = 'visible';
        addTxForm.setAttribute('aria-hidden', 'false');
    }
    else { // not hidden
        addTxForm.style['overflow'] = 'hidden';
        addTxForm.style['max-height'] = '0px';
        //await sleep(300);
        addTxForm.setAttribute('aria-hidden', 'true');
    }
}

/**
 * Toggles the Add Transfer form visibility. If it is hidden, it will slide down into view, if it is
 * visible, it will slide up out of view.
 */
async function toggleAddTransferVisibility() {
    if (addTransferForm.style['max-height'] === '0px') { // hidden
        if (addTxForm.style['max-height'] !== '0px') { // if tx form is showing, hide it
            toggleAddTxVisibility();
        }
        addTransferForm.style['max-height'] = addTransferFormMaxHeight;
        //await sleep(300);
        addTransferForm.style['overflow'] = 'visible';
        addTransferForm.setAttribute('aria-hidden', 'false');
    }
    else { // not hidden
        addTransferForm.style['overflow'] = 'hidden';
        addTransferForm.style['max-height'] = '0px';
        //await sleep(300);
        addTransferForm.setAttribute('aria-hidden', 'true');
    }
}

/**
 * Toggles the Account Filters visibility. If it is hidden, it will slide down into view, if it is
 * visible, it will slide up out of view.
 */
async function toggleAccountFiltersVisibility() {
    if (accountFilters.style['max-height'] === '0px') { // hidden
        if (txFilters.style['max-height'] !== '0px') { // if tx filters is showing, hide it
            toggleTxFiltersVisibility();
        }
        accountFilters.style['max-height'] = accountFiltersMaxHeight;
        //await sleep(300);
        accountFilters.style['overflow'] = 'visible';
        accountFilters.setAttribute('aria-hidden', 'false');
    }
    else { // not hidden
        accountFilters.style['overflow'] = 'hidden';
        accountFilters.style['max-height'] = '0px';
        //await sleep(300);
        accountFilters.setAttribute('aria-hidden', 'true');
    }
}

/**
 * Toggles the Tx Filters visibility. If it is hidden, it will slide down into view, if it is
 * visible, it will slide up out of view.
 */
async function toggleTxFiltersVisibility() {
    if (txFilters.style['max-height'] === '0px') { // hidden
        if (accountFilters.style['max-height'] !== '0px') { // if accounts filters is showing, hide it
            toggleAccountFiltersVisibility();
        }
        txFilters.style['max-height'] = txFiltersMaxHeight;
        //await sleep(300);
        txFilters.style['overflow'] = 'visible';
        txFilters.setAttribute('aria-hidden', 'false');
    }
    else { // not hidden
        txFilters.style['overflow'] = 'hidden';
        txFilters.style['max-height'] = '0px';
        //await sleep(300);
        txFilters.setAttribute('aria-hidden', 'true');
    }
}

function toggleAccountsTableVisibility() {
    var container = gebi('accounts-table-container');
    if (container.style.maxWidth === '') {
        container.style.maxWidth = '0px';
    }
    else {
        container.style.maxWidth = null;
    }
}

function toggleAddTxCredit() {
    var btn = gebi('add-tx-credit');
    if (btn.textContent === 'Credit') {
        btn.textContent = 'Debit';
        btn.classList.remove('btn-green-1');
        btn.classList.add('btn-red-1');
    }
    else {
        btn.textContent = 'Credit';
        btn.classList.add('btn-green-1');
        btn.classList.remove('btn-red-1');
    }
}

function toggleTxFilterDebit() {
    var btn = gebi('tx-filters-debit-btn');
    if (btn.textContent === 'Both') {
        btn.textContent = 'Credit';
        btn.classList.remove('btn-orange-1');
        btn.classList.add('btn-green-1');
    }
    else if (btn.textContent === 'Credit') {
        btn.textContent = 'Debit';
        btn.classList.remove('btn-green-1');
        btn.classList.add('btn-red-1');
    }
    else {
        btn.textContent = 'Both';
        btn.classList.remove('btn-red-1');
        btn.classList.add('btn-orange-1');
    }

    refreshTxTable();
}

function setAddTxDateError(text) {
    gebi('add-tx-date').classList.add('icon-input-error');
    var tt = gebi('add-tx-date-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTxDateError() {
    gebi('add-tx-date').classList.remove('icon-input-error');
    var tt = gebi('add-tx-date-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTxChargeError(text) {
    gebi('add-tx-charge').classList.add('icon-input-error');
    var tt = gebi('add-tx-charge-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTxChargeError() {
    gebi('add-tx-charge').classList.remove('icon-input-error');
    var tt = gebi('add-tx-charge-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTxPartyError(text) {
    gebi('add-tx-party').classList.add('icon-input-error');
    var tt = gebi('add-tx-party-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTxPartyError() {
    gebi('add-tx-party').classList.remove('icon-input-error');
    var tt = gebi('add-tx-party-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTxAccountError(text) {
    gebi('add-tx-account').classList.add('icon-input-error');
    var tt = gebi('add-tx-account-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTxAccountError() {
    gebi('add-tx-account').classList.remove('icon-input-error');
    var tt = gebi('add-tx-account-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTxAltDateError(text) {
    gebi('add-tx-alt-date').classList.add('icon-input-error');
    var tt = gebi('add-tx-alt-date-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTxAltDateError() {
    gebi('add-tx-alt-date').classList.remove('icon-input-error');
    var tt = gebi('add-tx-alt-date-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTxNotesError(text) {
    gebi('add-tx-notes').classList.add('icon-input-error');
    var tt = gebi('add-tx-notes-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTxNotesError() {
    gebi('add-tx-notes').classList.remove('icon-input-error');
    var tt = gebi('add-tx-notes-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function clearAddTxErrors() {
    clearAddTxDateError();
    clearAddTxChargeError();
    clearAddTxPartyError();
    clearAddTxAccountError();
    clearAddTxAltDateError();
    clearAddTxNotesError();
}

function resetAddTxForm() {
    clearAddTxErrors();
    gebi('add-tx-date').value = '';
    gebi('add-tx-charge').value = '';
    gebi('add-tx-party').value = '';
    gebi('add-tx-account').value = '';
    gebi('add-tx-alt-date').value = '';
    gebi('add-tx-notes').value = '';
    gebi('add-tx-credit').textContent = 'Debit';
    gebi('add-tx-credit').classList.add('btn-red-1');
    gebi('add-tx-credit').classList.remove('btn-green-1');
}

function setAddTransferDateError(text) {
    gebi('add-transfer-date').classList.add('icon-input-error');
    var tt = gebi('add-transfer-date-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTransferDateError() {
    gebi('add-transfer-date').classList.remove('icon-input-error');
    var tt = gebi('add-transfer-date-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTransferFromError(text) {
    gebi('add-transfer-from').classList.add('icon-input-error');
    var tt = gebi('add-transfer-from-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTransferFromError() {
    gebi('add-transfer-from').classList.remove('icon-input-error');
    var tt = gebi('add-transfer-from-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTransferToError(text) {
    gebi('add-transfer-to').classList.add('icon-input-error');
    var tt = gebi('add-transfer-to-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTransferToError() {
    gebi('add-transfer-to').classList.remove('icon-input-error');
    var tt = gebi('add-transfer-to-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTransferChargeError(text) {
    gebi('add-transfer-charge').classList.add('icon-input-error');
    var tt = gebi('add-transfer-charge-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTransferChargeError() {
    gebi('add-transfer-charge').classList.remove('icon-input-error');
    var tt = gebi('add-transfer-charge-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setAddTransferNotesError(text) {
    gebi('add-transfer-notes').classList.add('icon-input-error');
    var tt = gebi('add-transfer-notes-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAddTransferNotesError() {
    gebi('add-transfer-notes').classList.remove('icon-input-error');
    var tt = gebi('add-transfer-notes-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function clearAddTransferErrors() {
    clearAddTransferDateError();
    clearAddTransferFromError();
    clearAddTransferToError();
    clearAddTransferChargeError();
    clearAddTransferNotesError();
}

function resetAddTransferForm() {
    clearAddTransferErrors();
    gebi('add-transfer-date').value = '';
    gebi('add-transfer-charge').value = '';
    gebi('add-transfer-from').value = '';
    gebi('add-transfer-to').value = '';
    gebi('add-transfer-notes').value = '';
}

function setAccountFilterDateError(text) {
    gebi('accounts-balance-as-of').classList.add('icon-input-error');
    var tt = gebi('accounts-balance-as-of-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearAccountFilterDateError() {
    gebi('accounts-balance-as-of').classList.remove('icon-input-error');
    var tt = gebi('accounts-balance-as-of-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function clearAccountFilterErrors() {
    clearAccountFilterDateError();
}

function setTxFilterAccountsError(text) {
    gebi('tx-filters-accounts-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-accounts-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterAccountsError() {
    gebi('tx-filters-accounts-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-accounts-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setTxFilterPartiesError(text) {
    gebi('tx-filters-parties-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-parties-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterPartiesError() {
    gebi('tx-filters-parties-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-parties-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setTxFilterNotesError(text) {
    gebi('tx-filters-notes-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-notes-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterNotesError() {
    gebi('tx-filters-notes-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-notes-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setTxFilterMinChargeError(text) {
    gebi('tx-filters-min-charge-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-min-charge-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterMinChargeError() {
    gebi('tx-filters-min-charge-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-min-charge-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setTxFilterMaxChargeError(text) {
    gebi('tx-filters-max-charge-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-max-charge-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterMaxChargeError() {
    gebi('tx-filters-max-charge-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-max-charge-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setTxFilterDateBeginError(text) {
    gebi('tx-filters-date-begin-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-date-begin-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterDateBeginError() {
    gebi('tx-filters-date-begin-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-date-begin-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setTxFilterDateEndError(text) {
    gebi('tx-filters-date-end-input').classList.add('icon-input-error');
    var tt = gebi('tx-filters-date-end-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearTxFilterDateEndError() {
    gebi('tx-filters-date-end-input').classList.remove('icon-input-error');
    var tt = gebi('tx-filters-date-end-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function clearTxFilterErrors() {
    clearTxFilterAccountsError();
    clearTxFilterPartiesError();
    clearTxFilterNotesError();
    clearTxFilterMinChargeError();
    clearTxFilterMaxChargeError();
    clearTxFilterDateBeginError();
    clearTxFilterDateEndError();
}

function resetTxFilters() {
    clearTxFilterErrors();

    // Set date-begin to 3 months ago
    var date = new Date();
    date.setMonth(date.getMonth() - 3);
    var dd = String(date.getDate()).padStart(2, '0');
    var mm = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
    var yyyy = date.getFullYear();
    date = yyyy + '-' + mm + '-' + dd;

    gebi('tx-filters-date-begin-input').value = date;
}

function submitAddTx() {
    var date = gebi('add-tx-date').value;
    var charge = gebi('add-tx-charge').value;
    var credit = gebi('add-tx-credit').classList.contains('btn-green-1') ? 'true' : 'false';
    var party = gebi('add-tx-party').value;
    var account = gebi('add-tx-account').value;
    var altDate = gebi('add-tx-alt-date').value;
    var notes = gebi('add-tx-notes').value;

    clearAddTxErrors();

    fetch('/transactions/add/', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'date': date, 'charge': charge, 'credit': credit, 'party': party, 'account': account,
            'alt_date': altDate, 'notes': notes})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'date') {
                        setAddTxDateError(data['data'][param].message);
                    }
                    else if (param === 'charge') {
                        setAddTxChargeError(data['data'][param].message);
                    }
                    else if (param === 'credit') {
                        makeErrorNotification(data['data'][param].message + ' (This is likely a bug)');
                    }
                    else if (param === 'party') {
                        setAddTxPartyError(data['data'][param].message);
                    }
                    else if (param === 'account') {
                        setAddTxAccountError(data['data'][param].message);
                    }
                    else if (param === 'alt_date') {
                        setAddTxAltDateError(data['data'][param].message);
                    }
                    else if (param === 'notes') {
                        setAddTxNotesError(data['data'][param].message);
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            makeSuccessNotification('Added transaction');
            resetAddTxForm();
            refreshAccountTable();
            refreshTxTable();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });
}

function submitAddTransfer() {
    var date = gebi('add-transfer-date').value;
    var charge = gebi('add-transfer-charge').value;
    var from = gebi('add-transfer-from').value;
    var to = gebi('add-transfer-to').value;
    var notes = gebi('add-transfer-notes').value;

    clearAddTransferErrors();

    fetch('/transactions/add/transfer/', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'date': date, 'charge': charge, 'from_account': from, 'to_account': to, 'notes': notes})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'date') {
                        setAddTransferDateError(data['data'][param].message);
                    }
                    else if (param === 'charge') {
                        setAddTransferChargeError(data['data'][param].message);
                    }
                    else if (param === 'from_account') {
                        setAddTransferFromError(data['data'][param].message);
                    }
                    else if (param === 'to_account') {
                        setAddTransferToError(data['data'][param].message);
                    }
                    else if (param === 'notes') {
                        setAddTransferNotesError(data['data'][param].message);
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            makeSuccessNotification('Added transfer');
            resetAddTransferForm();
            refreshAccountTable();
            refreshTxTable();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });
}

/**
 * Fetch accounts and balances from the server and populate the table
 */
function refreshAccountTable() {
    clearAccountFilterErrors();

    var date = gebi('accounts-balance-as-of').value;
    fetch('/accounts/balance/?' + new URLSearchParams({'date': date}), {
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching accounts: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message'] + ' (Check your account filters)');
                for (var param in data['data']) {
                    if (param === 'date') {
                        setAccountFilterDateError(data['data'][param]['message'])
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred fetching accounts, see server logs for details');
            }
        }
        else {
            clearAccountTable();
            var result = data['result'];
            var accountBalances = []
            for (var key in result) {
                if (result[key]['account']['hide'] && Number(result[key]['balance']) == 0) {
                    // If an account is marked as hidden and the balance is 0, don't show it in the table
                    continue;
                }
                accountBalances.push({'account': result[key]['account'], 'balance': result[key]['balance']});
            }
            accountBalances.sort(function(a, b) {return a['account']['name'].localeCompare(b['account']['name'])})
            for (var index = 0; index < accountBalances.length; index++) {
                addAccountToTable(accountBalances[index]['account'], accountBalances[index]['balance']);
            }
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching accounts, see server logs for details');
    });
}

/*
 * Remove all accounts from the table
 */
function clearAccountTable() {
    var tbody = gebi('accounts-table-body');
    var newTbody = document.createElement('tbody');
    newTbody.id = 'accounts-table-body';
    tbody.replaceWith(newTbody);
}

var currencyFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits: 4,
});

function addAccountToTable(account, balance) {
    var row = document.createElement('tr');
    var id = account['id'];
    row.id = 'account-' + id;
    var name = account['name'];
    balance = currencyFormatter.format(balance);

    row.innerHTML = `
        <td>${name}</td>
        <td>${balance}</td>
    `
    gebi('accounts-table-body').appendChild(row);
}

function refreshTxTable() {
    var accounts = gebi('tx-filters-accounts-input').value;
    var parties = gebi('tx-filters-parties-input').value;
    var notes = gebi('tx-filters-notes-input').value;
    var minCharge = gebi('tx-filters-min-charge-input').value;
    var maxCharge = gebi('tx-filters-max-charge-input').value;
    var debit = '';
    var dateBegin = gebi('tx-filters-date-begin-input').value;
    var dateEnd = gebi('tx-filters-date-end-input').value;

    var debitBtn = gebi('tx-filters-debit-btn');
    if (debitBtn.classList.contains('btn-red-1')) {
        debit = true;
    }
    else if (debitBtn.classList.contains('btn-green-1')) {
        debit = false;
    }

    clearTxFilterErrors();

    fetch('/transactions/get/?' + new URLSearchParams(
        {'accounts': accounts, 'parties': parties, 'notes_contain': notes,
        'charge_begin': minCharge, 'charge_end': maxCharge, 'debit': debit,
        'date_begin': dateBegin, 'date_end': dateEnd}),
    {
        method: 'GET'
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching accounts: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message'] + ' (Check your account filters)');
                for (var param in data['data']) {
                    if (param === 'accounts') {
                        setTxFilterAccountsError(data['data'][param]['message']);
                    }
                    else if (param === 'parties') {
                        setTxFilterPartiesError(data['data'][param]['message'])
                    }
                    else if (param === 'notes') {
                        setTxFilterNotesError(data['data'][param]['message']);
                    }
                    else if (param === 'charge_begin') {
                        setTxFilterMinChargeError(data['data'][param]['message']);
                    }
                    else if (param === 'charge_end') {
                        setTxFilterMaxChargeError(data['data'][param]['message']);
                    }
                    else if (param === 'debit') {
                        makeErrorNotification(data['data'][param]['message'] + ' (This is likely a bug)');
                    }
                    else if (param === 'date_begin') {
                        setTxFilterDateBeginError(data['data'][param]['message']);
                    }
                    else if (param === 'date_end') {
                        setTxFilterDateEndError(data['data'][param]['message']);
                    }
                    else {
                        makeErrorNotification('An unknown parameter error occurred, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred fetching accounts, see server logs for details');
            }
        }
        else {
            clearTxTable();
            for (var index in data['result']) {
                addTxToTable(data['result'][index]);
            }
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching accounts, see server logs for details');
    });
}

/**
 * Remove all transactions from table
 */
function clearTxTable() {
    var tbody = gebi('txs-table-body');
    var newTbody = document.createElement('tbody');
    newTbody.id = 'txs-table-body';
    tbody.replaceWith(newTbody);
}

function addTxToTable(tx) {
    var row = createTxTableRow(tx);
    var id = tx['id'];
    gebi('txs-table-body').appendChild(row);
    gebi('tx-' + id + '-edit').addEventListener('click', switchToEditTx);
    gebi('tx-' + id + '-delete').addEventListener('click', deleteTx);
}

function createTxTableRow(tx) {
    var date = tx['date'];
    var alt_date = tx['alt_date'] ? 'Alt ' + tx['alt_date'] : '';
    var debit = Number(tx['charge']) < 0;
    var charge = currencyFormatter.format(tx['charge']);
    var party = tx['party'] ? tx['party']['name'] : '';
    var account = tx['account']['name']
    var notes = tx['notes'] ? tx['notes'] : ''

    var isChargeNegative = Number(tx['charge']) < 0;
    var chargeClass = '';
    if (isChargeNegative) {
        chargeClass = 'tx-data-table-charge-negative'
    }
    else {
        chargeClass = 'tx-data-table-charge-positive'
    }

    var row = document.createElement('tr');
    var id = tx['id'];
    row.id = 'tx-' + id;

    row.innerHTML = `
        <td class="tx-data-table-column-1">
            <span id="tx-${id}-date" title="${alt_date}">${date}</span>
        </td>
        <td class="tx-data-table-column-2 ${chargeClass}">
            <span id="tx-${id}-charge">${charge}</span>
        </td>
        <td class="tx-data-table-column-3">
            <span id="tx-${id}-party">${party}</span>
        </td>
        <td class="tx-data-table-column-4">
            <span id="tx-${id}-account">${account}</span>
        </td>
        <td class="tx-data-table-column-5">
            <span id="tx-${id}-notes">${notes}</span>
        </td>
        <td class="tx-data-table-column-6">
            <div class="action-btn-group">
                <div id="tx-${id}-edit" class="action-btn-group-btn action-btn-group-first action-btn-group-blue-1" title="Edit">
                    <i class="icon icon-pencil"></i>
                </div>
                <div id="tx-${id}-delete" class="action-btn-group-btn action-btn-group-last action-btn-group-red-1" title="Delete">
                    <i class="icon icon-trash"></i>
                </div>
            </div>
        </td>
    `;
    return row;
}

function switchToEditTx(event) {
    // tx-{id}-edit
    var id = event.currentTarget.id.slice(3, -5);
    var row = gebi('tx-' + id);
    var date = gebi('tx-' + id + '-date').textContent;
    var altDate = gebi('tx-' + id + '-date').title;
    var charge = gebi('tx-' + id + '-charge').textContent;
    var party = gebi('tx-' + id + '-party').textContent;
    var account = gebi('tx-' + id + '-account').textContent;
    var notes = gebi('tx-' + id + '-notes').textContent;

    charge = Number(charge.replace(/[^0-9.-]+/g,""));
    var credit = charge >= 0;
    if (!credit) {
        charge *= -1;
    }
    charge = charge.toString();

    editingTxs[id] = row.cloneNode(true);

    var editRow = document.createElement('tr');
    editRow.id = 'tx-' + id;
    editRow.innerHTML = `
        <td class="tx-data-table-column-1">
            <div class="flex">
                <div class="icon-input-div edit-tx-input-font-color tooltip">
                    <i class="icon icon-calendar-1 icon-input-icon"></i>
                    <input class="icon-input edit-tx-input" id="edit-tx-${id}-date" type="text" placeholder="Date*" value="${date}">
                    <span class="tooltip-text" id="edit-tx-${id}-date-tooltip">Default Text</span>
                </div>
                <div class="gap"></div>
                <div class="icon-input-div edit-tx-input-font-color tooltip">
                    <i class="icon icon-calendar-1 icon-input-icon"></i>
                    <input class="icon-input edit-tx-input" id="edit-tx-${id}-alt-date" type="text" placeholder="Alt Date" value="${altDate}">
                    <span class="tooltip-text" id="edit-tx-${id}-alt-date-tooltip">Default Text</span>
                </div>
            </div>
        </td>
        <td class="tx-data-table-column-2">
            <div class="flex">
                <div class="icon-input-div edit-tx-input-font-color tooltip">
                    <i class="icon icon-dollar-symbol icon-input-icon"></i>
                    <input class="icon-input edit-tx-input" id="edit-tx-${id}-charge" type="text" placeholder="Charge*" value="${charge}">
                    <span class="tooltip-text" id="edit-tx-${id}-charge-tooltip">Default Text</span>
                </div>
                <div class="gap"></div>
                <button class="btn btn-green-1" id="edit-tx-${id}-credit">Credit</button>
            </div>
        </td>
        <td class="tx-data-table-column-3">
            <div class="flex">
                <div class="icon-input-div edit-tx-input-font-color tooltip">
                    <i class="icon icon-user icon-input-icon"></i>
                    <input class="icon-input edit-tx-input" id="edit-tx-${id}-party" type="text" placeholder="Party" value="${party}">
                    <span class="tooltip-text" id="edit-tx-${id}-party-tooltip">Default Text</span>
                </div>
            </div>
        </td>
        <td class="tx-data-table-column-4">
            <div class="flex">
                <div class="icon-input-div edit-tx-input-font-color tooltip">
                    <i class="icon icon-email icon-input-icon"></i>
                    <input class="icon-input edit-tx-input" id="edit-tx-${id}-account" type="text" placeholder="Account" value="${account}">
                    <span class="tooltip-text" id="edit-tx-${id}-account-tooltip">Default Text</span>
                </div>
            </div>
        </td>
        <td class="tx-data-table-column-5">
            <div class="flex">
                <div class="icon-input-div edit-tx-input-font-color tooltip">
                    <i class="icon icon-notes icon-input-icon"></i>
                    <input class="icon-input edit-tx-input" id="edit-tx-${id}-notes" type="text" placeholder="Notes" value="${notes}">
                    <span class="tooltip-text" id="edit-tx-${id}-notes-tooltip">Default Text</span>
                </div>
            </div>
        </td>
        <td class="tx-data-table-column-6">
            <div class="action-btn-group">
                <div id="edit-tx-${id}-save" class="action-btn-group-btn action-btn-group-first action-btn-group-green-3" title="Save">
                    <i class="icon icon-floppy"></i>
                </div>
                <div id="edit-tx-${id}-cancel" class="action-btn-group-btn action-btn-group-last action-btn-group-red-1" title="Cancel">
                    <i class="icon icon-cancel"></i>
                </div>
            </div>
        </td>
    `;
    row.replaceWith(editRow);

    // Set Credit button
    if (!credit) {
        toggleEditTxCreditButton(id);
    }

    // Event listeners
    gebi('edit-tx-' + id + '-credit').addEventListener('click', toggleEditTxCreditButtonEvent);

    gebi('edit-tx-' + id + '-date').addEventListener('change', clearEditTxDateErrorEvent);
    gebi('edit-tx-' + id + '-alt-date').addEventListener('change', clearEditTxAltDateErrorEvent);
    gebi('edit-tx-' + id + '-charge').addEventListener('change', clearEditTxChargeErrorEvent);
    gebi('edit-tx-' + id + '-party').addEventListener('change', clearEditTxPartyErrorEvent);
    gebi('edit-tx-' + id + '-account').addEventListener('change', clearEditTxAccountErrorEvent);
    gebi('edit-tx-' + id + '-notes').addEventListener('change', clearEditTxNotesErrorEvent);

    gebi('edit-tx-' + id + '-save').addEventListener('click', saveEditTx);
    gebi('edit-tx-' + id + '-cancel').addEventListener('click', cancelEditTx);

    // Autocomplete
    if (accountAutocompleteSuggestions) {
        setupAutocomplete(gebi('edit-tx-' + id + '-account'), accountAutocompleteSuggestions);
    }
    if (partyAutocompleteSuggestions) {
        setupAutocomplete(gebi('edit-tx-' + id + '-party'), partyAutocompleteSuggestions);
    }

    // Clear errors
    clearEditTxErrors(id);
}

function toggleEditTxCreditButton(id) {
    var btn = gebi('edit-tx-' + id + '-credit');
    if (btn.textContent === 'Credit') {
        btn.textContent = 'Debit';
        btn.classList.remove('btn-green-1');
        btn.classList.add('btn-red-1');
    }
    else {
        btn.textContent = 'Credit';
        btn.classList.add('btn-green-1');
        btn.classList.remove('btn-red-1');
    }
}

function toggleEditTxCreditButtonEvent(event) {
    // edit-tx-{id}-credit
    var id = event.currentTarget.id.slice(8, -7);
    toggleEditTxCreditButton(id);
}

function setIconInputError(elem, text) {
    elem.classList.add('icon-input-error');
    var tt = gebi(elem.id + '-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

function clearIconInputError(elem) {
    elem.classList.remove('icon-input-error');
    var tt = gebi(elem.id + '-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

function setEditTxDateError(id, text) { setIconInputError(gebi('edit-tx-' + id + '-date'), text); }
function clearEditTxDateError(id) { clearIconInputError(gebi('edit-tx-' + id + '-date')); }
function clearEditTxDateErrorEvent(event) { clearEditTxDateError(event.currentTarget.id.slice(8, -5)); }

function setEditTxAltDateError(id, text) { setIconInputError(gebi('edit-tx-' + id + '-alt-date'), text); }
function clearEditTxAltDateError(id) { clearIconInputError(gebi('edit-tx-' + id + '-alt-date')); }
function clearEditTxAltDateErrorEvent(event) { clearEditTxAltDateError(event.currentTarget.id.slice(8, -9)); }

function setEditTxChargeError(id, text) { setIconInputError(gebi('edit-tx-' + id + '-charge'), text); }
function clearEditTxChargeError(id) { clearIconInputError(gebi('edit-tx-' + id + '-charge')); }
function clearEditTxChargeErrorEvent(event) { clearEditTxChargeError(event.currentTarget.id.slice(8, -7)); }

function setEditTxPartyError(id, text) { setIconInputError(gebi('edit-tx-' + id + '-party'), text); }
function clearEditTxPartyError(id) { clearIconInputError(gebi('edit-tx-' + id + '-party')); }
function clearEditTxPartyErrorEvent(event) { clearEditTxPartyError(event.currentTarget.id.slice(8, -6)); }

function setEditTxAccountError(id, text) { setIconInputError(gebi('edit-tx-' + id + '-account'), text); }
function clearEditTxAccountError(id) { clearIconInputError(gebi('edit-tx-' + id + '-account')); }
function clearEditTxAccountErrorEvent(event) { clearEditTxAccountError(event.currentTarget.id.slice(8, -8)); }

function setEditTxNotesError(id, text) { setIconInputError(gebi('edit-tx-' + id + '-notes'), text); }
function clearEditTxNotesError(id) { clearIconInputError(gebi('edit-tx-' + id + '-notes')); }
function clearEditTxNotesErrorEvent(event) { clearEditTxNotesError(event.currentTarget.id.slice(8, -6)); }

function clearEditTxErrors(id) {
    clearEditTxDateError(id);
    clearEditTxAltDateError(id);
    clearEditTxChargeError(id);
    clearEditTxPartyError(id);
    clearEditTxAccountError(id);
    clearEditTxNotesError(id);
}

/**
 * Save the changes to the transaction by sending the new data to the server and updating the values with the reply.
 * If there are errors, the fields will be highlighted and tooltip the same as when adding a new transaction.
 *
 * @param {Event} event - The click event from the save button.
 */
function saveEditTx(event) {
    // edit-tx-{id}-save
    var id = event.currentTarget.id.slice(8, -5);

    var date = gebi('edit-tx-' + id + '-date').value;
    var charge = gebi('edit-tx-' + id + '-charge').value;
    var credit = gebi('edit-tx-' + id + '-credit').classList.contains('btn-green-1') ? true : false;
    var party = gebi('edit-tx-' + id + '-party').value;
    var account = gebi('edit-tx-' + id + '-account').value;
    var altDate = gebi('edit-tx-' + id + '-alt-date').value;
    var notes = gebi('edit-tx-' + id + '-notes').value;

    if (!credit) {
        charge = Number(charge);
        charge *= -1;
        charge = charge.toString();
    }

    // TODO Why in the world is this needed? Not needed for any other requests
    var txId = id;

    clearEditTxErrors(id);

    fetch('/transactions/' + id + '/', {
        method: 'PUT',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'date': date, 'charge': charge, 'credit': credit, 'party': party, 'account': account,
            'alt_date': altDate, 'notes': notes})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'date') {
                        setEditTxDateError(txId, data['data'][param].message);
                    }
                    else if (param === 'charge') {
                        setEditTxChargeError(txId, data['data'][param].message);
                    }
                    else if (param === 'party') {
                        setEditTxPartyError(txId, data['data'][param].message);
                    }
                    else if (param === 'account') {
                        setEditTxAccountError(txId, data['data'][param].message);
                    }
                    else if (param === 'alt_date') {
                        setEditTxAltDateError(txId, data['data'][param].message);
                    }
                    else if (param === 'notes') {
                        setEditTxNotesError(txId, data['data'][param].message);
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            var tx = data['result'];
            var newRow = createTxTableRow(tx);
            var id = tx['id'];
            delete editingTxs[id];
            row = gebi('tx-' + id);
            row.replaceWith(newRow);
            gebi('tx-' + id + '-edit').addEventListener('click', switchToEditTx);
            gebi('tx-' + id + '-delete').addEventListener('click', deleteTx);

            refreshAccountTable();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });
}

/**
 * Cancel editing a transaction and return the row to it's original state.
 *
 * @param {Event} event - The click event from the cancel button.
 */
function cancelEditTx(event) {
    // edit-tx-{id}-cancel
    var id = event.currentTarget.id.slice(8, -7);
    row = gebi('tx-' + id);
    row.replaceWith(editingTxs[id])
    delete editingTxs[id];
    gebi('tx-' + id + '-edit').addEventListener('click', switchToEditTx);
    gebi('tx-' + id + '-delete').addEventListener('click', deleteTx);
}

/**
 * Delete a transaction.
 *
 * @param {Event} event - The click event from the delete button.
 */
function deleteTx(event) {
    // tx-{id}-delete
    var id = event.currentTarget.id.slice(3, -7);

    fetch('/transactions/' + id + '/', {
        method: 'DELETE'
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'resource') {
                makeErrorNotification(data['message']);
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            var row = gebi('tx-' + id);
            row.remove();
            refreshAccountTable();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });
}

async function refreshAutocompleteSuggestions() {
    accountAutocompleteSuggestions = await getAccountAutocompleteSuggestions();
    partyAutocompleteSuggestions = await getPartyAutocompleteSuggestions();
}

async function setupAutocompleteTx() {
    await refreshAutocompleteSuggestions();
    if (accountAutocompleteSuggestions) {
        setupAutocomplete(gebi('add-tx-account'), accountAutocompleteSuggestions);
        setupAutocomplete(gebi('add-transfer-from'), accountAutocompleteSuggestions);
        setupAutocomplete(gebi('add-transfer-to'), accountAutocompleteSuggestions);
        setupAutocomplete(gebi('tx-filters-accounts-input'), accountAutocompleteSuggestions, true);
    }

    if (partyAutocompleteSuggestions) {
        setupAutocomplete(gebi('add-tx-party'), partyAutocompleteSuggestions);
        setupAutocomplete(gebi('tx-filters-parties-input'), partyAutocompleteSuggestions, true);
    }
}

async function getAccountAutocompleteSuggestions() {
    var suggestions = null;
    await fetch('/accounts/get/', {
        method: 'GET',
        headers: { 'content-type': 'application/json' }
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching accounts: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification('A parameter error occurred fetching accounts: ' + data['message']
                + ' (This is likely a bug)');
            }
            else {
                makeErrorNotification('An unknown error occurred fetching accounts, see server logs for details');
            }
        }
        else {
            var accounts = data['result']['accounts'];
            // Extract just the names
            suggestions = accounts.map(function(x) {return x['name'];});
            suggestions.sort();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching accounts, see server logs for details');
    });
    return suggestions;
}

async function getPartyAutocompleteSuggestions() {
    var suggestions = null;
    await fetch('/parties/get/', {
        method: 'GET',
        headers: { 'content-type': 'application/json' }
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching parties: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification('A parameter error occurred fetching parties: ' + data['message']
                + ' (This is likely a bug)');
            }
            else {
                makeErrorNotification('An unknown error occurred fetching parties, see server logs for details');
            }
        }
        else {
            var parties = data['result']['parties'];
            // Extract just the names
            suggestions = parties.map(function(x) {return x['name'];});
            suggestions.sort();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching parties, see server logs for details');
    });
    return suggestions;
}
