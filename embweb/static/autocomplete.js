/**
 * Adds autocomplete to an input.
 *
 * @param {Element} input - The input to add autocomplete to.
 * @param {Array[string]} values - The values to autocomplete with.
 * @param {bool} multi - True if multiple comma-separated entries are permitted.
 */
function setupAutocomplete(input, values, multi) {
    var currentFocus;

    input.classList.add('autocomplete-ignore-clear-click');
    input.addEventListener('focus', suggestAutocomplete);
    input.addEventListener('input', suggestAutocomplete);

    function suggestAutocomplete() {
        closeAllLists();
        currentFocus = -1;
        var listDiv = createAutocompleteList(this.id);
        this.parentNode.appendChild(listDiv);

        var val;
        if (multi) {
            var entries = this.value.split(',')
            val = entries[entries.length - 1];
            val = val.trim();
        }
        else {
            val = this.value;
        }

        if (val.length === 0) {
            var limit = Math.min(5, values.length);
            for (var i = 0; i < limit; i++) {
                addAutocompleteItemToList(listDiv, values[i], 0);
            }
        }
        else {
            for (var i = 0; i < values.length; i++) {
                if (values[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    addAutocompleteItemToList(listDiv, values[i], val.length)
                }
            }
        }
    }

    function createAutocompleteList(id) {
        var listDiv = document.createElement("div");
        listDiv.id = id + '-autocomplete-list';
        listDiv.classList.add('autocomplete-items');
        return listDiv;
    }

    function addAutocompleteItemToList(listDiv, suggestionText, matchingChars) {
        var listItemDiv = document.createElement('div');
        if (matchingChars === 0) {
          listItemDiv.innerHTML = suggestionText;
        }
        else {
          listItemDiv.innerHTML = "<strong>" + suggestionText.substr(0, matchingChars) + "</strong>";
          listItemDiv.innerHTML += suggestionText.substr(matchingChars);
        }
        // insert a input field that will hold the current item's unaltered value
        listItemDiv.innerHTML += "<input type='hidden' value='" + suggestionText + "'>";
        // fill the input with the selection on click
        listItemDiv.addEventListener("click", function(e) {
            if (multi) {
                var entries = input.value.split(',');
                var soFar = '';
                for (var entryIndex = 0; entryIndex < entries.length -1; entryIndex++) {
                  soFar += entries[entryIndex].trim() + ', ';
                }
                input.value = soFar + this.getElementsByTagName("input")[0].value;
            }
            else {
                input.value = this.getElementsByTagName("input")[0].value;
            }
            // Move this item to the front of the list
            var selectedValue = this.getElementsByTagName("input")[0].value;
            // Remove
            values.splice(values.indexOf(selectedValue), 1);
            // Add to front
            values.unshift(selectedValue);

            closeAllLists();
            //input.focus();
            event = new Event('change');
            input.dispatchEvent(event);

        });
        // Prevent the input from losing focus when a suggestion is clicked
        listItemDiv.addEventListener('mousedown', function(e) {
            e.preventDefault();
        });
        listDiv.appendChild(listItemDiv);
    }

    input.addEventListener('keydown', function(e) {
        var list = document.getElementById(this.id + '-autocomplete-list');
        if (!list) {
          return;
        }
        var listItems = list.getElementsByTagName('div');
        if (e.keyCode == 40) { // Down
          currentFocus++;
          addActive(listItems);
        } else if (e.keyCode == 38) { // Up
          currentFocus--;
          addActive(listItems);
        } else if (e.keyCode == 13) { // Enter
          e.preventDefault(); // Prevent Form submission
          if (currentFocus > -1) {
              listItems[currentFocus].click();
          }
        }
    });

    function addActive(listItems) {
        /*a function to classify an item as "active":*/
        if (!listItems) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(listItems);
        if (currentFocus >= listItems.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (listItems.length - 1);
        listItems[currentFocus].classList.add('autocomplete-active');
    }

    function removeActive(listItems) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < listItems.length; i++) {
        listItems[i].classList.remove('autocomplete-active');
        }
    }

    function closeAllLists(element) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var items = document.getElementsByClassName('autocomplete-items');
        for (var i = 0; i < items.length; i++) {
            if (element != items[i] && element != input) {
            items[i].remove();
            }
        }
    }

    // When the input loses focus, close the list
    input.addEventListener('blur', function() {
        closeAllLists();
    })
}
