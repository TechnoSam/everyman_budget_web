var notifCount = 0;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function makeNotification(notif) {
    document.body.appendChild(notif);
    var compStyle = window.getComputedStyle(notif);
    var marginTop = Number(compStyle.marginTop.slice(0, -2));
    var marginRight = Number(compStyle.marginRight.slice(0, -2));
    var width = notif.getBoundingClientRect().width + marginRight;
    width = width.toString() + 'px';
    var height = notif.getBoundingClientRect().height + marginTop;
    height = height.toString() + 'px';
    notif.style.left = 'calc(100vw - ' + width + ')';
    notif.style.top = window.getComputedStyle(notif).top;
    var notifs = document.querySelectorAll('.notification');
    for (var i = 0; i < notifs.length; i++) {
      var old = notifs[i];
      if (old !== notif) {
        var oldTop = old.style.top;
        var newTop = 'calc(' + oldTop + ' + ' + height + ')';
        old.style.top = newTop;
      }
    }
    await sleep(300);
    notif.style.transition = "top 0.3s";
    await sleep(10000);
    notif.style.transition = 'opacity 0.2s'
    notif.style.opacity = 0;
    await sleep(200);
    notif.remove();
}

async function makeErrorNotification(text) {
    var notif = document.createElement('div');
    notif.id = 'notif-' + notifCount;
    notifCount += 1;
    notif.classList.add('notification', 'notification-error');
    notif.innerHTML = `
        <i class="icon icon-attention icon-notif"></i>
        <div class="notif-text-area">
          <span>${text}</span>
        </div>
    `
    makeNotification(notif);
}

async function makeSuccessNotification(text) {
    var notif = document.createElement('div');
    notif.id = 'notif-' + notifCount;
    notifCount += 1;
    notif.classList.add('notification', 'notification-success');
    notif.innerHTML = `
        <i class="icon icon-ok-circled icon-notif"></i>
        <div class="notif-text-area">
          <span>${text}</span>
        </div>
    `
    makeNotification(notif);
}

async function makeInfoNotification(text) {
    var notif = document.createElement('div');
    notif.id = 'notif-' + notifCount;
    notifCount += 1;
    notif.classList.add('notification', 'notification-info');
    notif.innerHTML = `
        <i class="icon icon-attention-circled icon-notif"></i>
        <div class="notif-text-area">
          <span>${text}</span>
        </div>
    `
    makeNotification(notif);
}
