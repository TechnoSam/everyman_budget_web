function gebi(id) {
    return document.getElementById(id);
}

// Listeners
// Show Add Account Form
gebi('add-account-btn').addEventListener('click', toggleAddAccountVisibility);

// Hidden Color Input
gebi('add-account-color-icon').addEventListener('click', showAddAccountColorPicker);
gebi('add-account-hidden-color-input').addEventListener('change', updateAddAccountColor);
gebi('add-account-color').addEventListener('change', updateAddAccountHiddenColor);

// Add Account Hide Button
gebi('add-account-hide-btn').addEventListener('click', toggleAddAccountHide);

// Add Account Submit Button
gebi('add-account-submit').addEventListener('click', submitAddAccount);

// Clear errors on edit
gebi('add-account-name').addEventListener('change', clearAddAccountNameErrors);
gebi('add-account-color').addEventListener('change', clearAddAccountColorErrors);

// Set max-height for animation
var addAccountForm = gebi('add-account-form');
var addAccountFormMaxHeight = addAccountForm.scrollHeight + "px";
addAccountForm.style['overflow'] = 'hidden';
addAccountForm.style['max-height'] = '0px';
addAccountForm.setAttribute('aria-hidden', 'true');

// Initialize Add Account Form
resetAddAccountForm();

// Fetch data
refreshAccountTable();

// Variable to store the table rows that are being edited so they can be replaced after saving or canceling
var editingAccounts = {};

/**
 * Toggles the Add Account form visibility. If it is hidden, it will slide down into view, if it is
 * visible, it will slide up out of view.
 */
async function toggleAddAccountVisibility() {
    if (addAccountForm.style['max-height'] === '0px') { // hidden
        addAccountForm.style['max-height'] = addAccountFormMaxHeight;
        await sleep(300);
        addAccountForm.style['overflow'] = 'visible';
        addAccountForm.setAttribute('aria-hidden', 'false');
    }
    else { // not hidden
        addAccountForm.style['overflow'] = 'hidden';
        addAccountForm.style['max-height'] = '0px';
        await sleep(300);
        addAccountForm.setAttribute('aria-hidden', 'true');
    }
}

/**
 * Shows the color picker for the Add Account form.
 */
function showAddAccountColorPicker() {
    gebi('add-account-hidden-color-input').click();
}

/**
 * Sets the Add Account color field from the hidden color input.
 *
 */
function updateAddAccountColor() {
    gebi('add-account-color').value = gebi('add-account-hidden-color-input').value;
    clearAddAccountColorErrors();
}

/**
 * Sets the Add Account hidden color input from the main color input.
 */
function updateAddAccountHiddenColor() {
    gebi('add-account-hidden-color-input').value = gebi('add-account-color').value;
}

/**
 * Toggle the state of the Add Account hide button.
 */
function toggleAddAccountHide() {
    var button = gebi('add-account-hide-btn');
    button.classList.toggle('btn-red-1');
    button.classList.toggle('btn-green-1');
    var icon = gebi('add-account-hide-btn-icon');
    icon.classList.toggle('icon-visibility');
    icon.classList.toggle('icon-invisible');
    var text = gebi('add-account-hide-btn-text');
    if (text.textContent === 'Visible') {
        text.textContent = 'Not Visible';
    }
    else {
        text.textContent = 'Visible';
    }
}

/**
 * Submits the data in the Add Account form to the server.
 *
 * If there are any errors, a notification will be created that explains the error. If there is an error with any of the
 * field data, each field will be highlighted and given a tooltip that explains the problem. If the account is submitted
 * successfully, a notification will be created and the table will be refreshed to show the change.
 */
function submitAddAccount() {
    var name = gebi('add-account-name').value;
    var color = gebi('add-account-color').value;
    var hide = gebi('add-account-hide-btn').classList.contains('btn-red-1') ? 'true' : 'false';

    clearAddAccountFormErrors();

    fetch('/accounts/add/', {
        method: 'POST',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'name': name, 'color': color, 'hide': hide})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'name') {
                        gebi('add-account-name').classList.add('icon-input-error');
                        setAddAccountNameTooltip(data['data'][param].message);
                    }
                    else if (param === 'color') {
                        gebi('add-account-color').classList.add('icon-input-error');
                        setAddAccountColorTooltip(data['data'][param].message);
                    }
                    else if (param === 'hide') {
                        makeErrorNotification(data['data'][param].message + ' (This is likely a bug)');
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            makeSuccessNotification('Added new account');
            resetAddAccountForm();
            refreshAccountTable();
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });

}

/**
 * Clear all errors in the Add Account form
 */
function clearAddAccountFormErrors() {
    gebi('add-account-name').classList.remove('icon-input-error');
    gebi('add-account-name').title = '';
    gebi('add-account-color').classList.remove('icon-input-error');
    gebi('add-account-color').title = '';
}

/**
 * Reset the Add Account form to it's initial state
 */
function resetAddAccountForm() {
    clearAddAccountFormErrors();
    clearAddAccountNameTooltip();
    clearAddAccountColorTooltip();
    gebi('add-account-name').value = '';
    gebi('add-account-color').value = '';
    if (gebi('add-account-hide-btn-text').textContent === 'Not Visible') {
        toggleAddAccountHide();
    }
}

/**
 * Set the tooltip for the Add Account name field
 */
function setAddAccountNameTooltip(text) {
    var tt = gebi('add-account-name-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

/**
 * Remove the Add Account name field tooltip
 */
function clearAddAccountNameTooltip() {
    var tt = gebi('add-account-name-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

/**
 * Set the tooltip for the Add Account color field
 */
function setAddAccountColorTooltip(text) {
    var tt = gebi('add-account-color-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

/**
 * Remove the Add Account color field tooltip
 */
function clearAddAccountColorTooltip() {
    var tt = gebi('add-account-color-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

/**
 * Clear the error on the Add Account name field
 */
function clearAddAccountNameErrors() {
    clearAddAccountNameTooltip();
    gebi('add-account-name').classList.remove('icon-input-error');
}

/**
 * Clear the error on the Add Account color field
 */
function clearAddAccountColorErrors() {
    clearAddAccountColorTooltip();
    gebi('add-account-color').classList.remove('icon-input-error');
}

/**
 * Fetch the accounts from the server and populate the table with the data
 */
function refreshAccountTable() {
    fetch('/accounts/get/', {
        method: 'GET',
        headers: { 'content-type': 'application/json' }
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred fetching accounts: ' + data['message']);
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification('A parameter error occurred fetching accounts: ' + data['message']
                + ' (This is likely a bug)');
            }
            else {
                makeErrorNotification('An unknown error occurred fetching accounts, see server logs for details');
            }
        }
        else {
            clearAccountTable();
            var accounts = data['result']['accounts'];
            accounts.sort(function(a, b) {return a['name'].localeCompare(b['name'])})
            for (var index = 0; index < accounts.length; index++) {
                addAccountToTable(accounts[index]);
            }
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error fetching accounts, see server logs for details');
    });
}

/**
 * Remove all accounts from the table
 */
function clearAccountTable() {
    var tbody = gebi('accounts-table-body');
    var newTbody = document.createElement('tbody');
    newTbody.id = 'accounts-table-body';
    tbody.replaceWith(newTbody);
}

/**
 * Adds a new account to the bottom of the table.
 *
 * @param {account} account - The account to add.
 */
function addAccountToTable(account) {
    var row = document.createElement('tr');
    var id = account['id'];
    row.id = 'account-' + id;
    var name = account['name'];
    var colorClass = account['color'] === null ? '' : 'data-table-color-swatch';
    var colorText = account['color'] === null ? '' : '#' + account['color'].toLowerCase();
    var colorStyle = account['color'] === null ? '' : 'style="background-color:#' + account['color'].toLowerCase() + '"';
    var hideIconClass = account['hide'] ? 'icon-invisible red-1' : 'icon-visibility green-1';
    row.innerHTML = `
        <td class="account-data-table-column-1">
            <span id="account-${id}-name">${name}</span>
        </td>
        <td class="account-data-table-column-2">
            <div class="flex-v-center">
                <span id="account-${id}-color" class="data-table-color-code">${colorText}</span>
                <div class="gap"></div>
                <div id="account-${id}-color-swatch" class="${colorClass}" ${colorStyle}></div>
            </div>
        </td>
        <td class="account-data-table-column-3 text-center">
            <i id="account-${id}-hide" class="icon data-table-icon ${hideIconClass}"></i>
        </td>
        <td class="account-data-table-column-4">
            <div class="action-btn-group">
                <div id="account-${id}-edit" class="action-btn-group-btn action-btn-group-first action-btn-group-last action-btn-group-blue-1" title='Edit "${name}"'>
                    <i class="icon icon-pencil"></i>
                </div>
            </div>
        </td>
    `
    gebi('accounts-table-body').appendChild(row);
    gebi('account-' + account['id'] + '-edit').addEventListener('click', switchToEdit);
}

/**
 * Switches the account's row in the table to "edit" mode.
 *
 * @param {event} event - The event fired from the edit button.
 */
function switchToEdit(event) {
    id = event.currentTarget.id.slice(8, -5);
    row = gebi('account-' + id);
    var name = gebi('account-' + id + '-name').textContent;
    var color = gebi('account-' + id + '-color').textContent;
    var hide = gebi('account-' + id + '-hide').classList.contains('icon-invisible');
    editingAccounts[id] = row.cloneNode(true);

    var editRow = document.createElement('tr');
    editRow.id = 'account-' + id;
    editRow.innerHTML = `
        <td class="account-data-table-column-1">
            <div class="icon-input-div edit-account-font-size edit-account-input-font-color tooltip">
                <i class="icon icon-title icon-input-icon"></i>
                <input class="icon-input add-account-input" id="edit-account-${id}-name" type="text" placeholder="Name*" value="${name}">
                <span class="tooltip-text" id="edit-account-${id}-name-tooltip">Default Text</span>
            </div>
        </td>
        <td class="account-data-table-column-2">
            <div class="edit-account-color-container">
                <input id="edit-account-${id}-hidden-color-input" class="hidden-color-input" type="color" tabindex="-1" value="${color}">
                <div class="icon-input-div edit-account-font-size edit-account-input-font-color tooltip">
                    <i class="icon icon-paint-board-and-brush icon-input-icon pointer" id="edit-account-${id}-color-icon"></i>
                    <input class="icon-input add-account-input" id="edit-account-${id}-color" type="text" placeholder="Color" value="${color}">
                    <span class="tooltip-text" id="edit-account-${id}-color-tooltip">Default Text</span>
                </div>
            </div>
        </td>
        <td class="text-center account-data-table-column-3">
            <button id="edit-account-${id}-hide-btn" class="btn btn-green-1 edit-account-font-size edit-account-hide-btn" id="edit-account-${id}-hide-btn">
            <i class="icon icon-visibility" id="edit-account-${id}-hide-btn-icon"></i> <span id="edit-account-${id}-hide-btn-text">Visible</span>
        </button>
        </td>
        <td class="account-data-table-column-4">
            <div class="action-btn-group">
                <div id="account-${id}-save" class="action-btn-group-btn action-btn-group-first action-btn-group-green-3" title="Save">
                    <i class="icon icon-floppy"></i>
                </div>
                <div id="account-${id}-cancel" class="action-btn-group-btn action-btn-group-last action-btn-group-red-1" title="Cancel">
                    <i class="icon icon-cancel"></i>
                </div>
            </div>
        </td>
    `
    row.replaceWith(editRow);

    // Event Listeners
    // Clear errors on name field when changed
    gebi('edit-account-' + id + '-name').addEventListener('change', clearEditAccountNameErrorEvent);
    // Clear errors on color fieled when changed
    gebi('edit-account-' + id + '-color').addEventListener('change', clearEditAccountColorErrorEvent);
    // Show the color picker when the icon is clicked
    gebi('edit-account-' + id + '-color-icon').addEventListener('click', showEditAccountColorPicker);
    // Change the hidden color input when the visible input changes
    gebi('edit-account-' + id + '-hidden-color-input').addEventListener('change', updateEditAccountColor);
    // Change visible color input when hidden input changes
    gebi('edit-account-' + id + '-color').addEventListener('change', updateEditAccountHiddenColor);
    // Toggle the visibility when the visibility button is clicked
    gebi('edit-account-' + id + '-hide-btn').addEventListener('click', toggleEditAccountVisibilityEvent);
    // Save the new state when the save button is clicked
    gebi('account-' + id + '-save').addEventListener('click', saveEditAccount);
    // Cancel the edit when the cancel button is clicked
    gebi('account-' + id + '-cancel').addEventListener('click', cancelEditAccount);

    // Initialize
    if (hide) {
        toggleEditAccountVisibility(id);
    }
    clearEditAccountNameTooltip(id);
    clearEditAccountColorTooltip(id);
}

/**
 * Clear any Edit Account name field error when the field is changed.
 *
 * @param {Event} event - The change event dispatched from the field.
 */
function clearEditAccountNameErrorEvent(event) {
    // edit-account-{id}-name
    var id = event.currentTarget.id.slice(13, -5);
    clearEditAccountNameError(id);
}

/**
 * Clear any Edit Account name field error.
 *
 * @param {string} id - The ID of the account to clear.
 */
function clearEditAccountNameError(id) {
    var elem = gebi('edit-account-' + id + '-name');
    elem.classList.remove('icon-input-error');
    clearEditAccountNameTooltip(id);
}

/**
 * Set the tooltip for an Edit Account name field.
 *
 * @param {string} id - The id of the account to set.
 * @param {string} text - The text to set the tooltip to.
 */
function setEditAccountNameTooltip(id, text) {
    var tt = gebi('edit-account-' + id + '-name-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

/**
 * Clear an Edit Account name field tooltip
 *
 * @param {string} id - The ID of the account to clear
 */
function clearEditAccountNameTooltip(id) {
    var tt = gebi('edit-account-' + id + '-name-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

/**
 * Clear any Edit Account color field error when the field is changed.
 *
 * @param {Event} event - The change event dispatched from the field.
 */
function clearEditAccountColorErrorEvent(event) {
    // edit-account-{id}-color
    var id = event.currentTarget.id.slice(13, -6);
    clearEditAccountColorError(id);
}

/**
 * Clear any Edit Account color field error.
 *
 * @param {string} id - The id of the account to clear.
 */
function clearEditAccountColorError(id) {
    var elem = gebi('edit-account-' + id + '-color');
    elem.classList.remove('icon-input-error');
    clearEditAccountColorTooltip(id);
}

/**
 * Set the tooltip for an Edit Account color field.
 *
 * @param {string} id - The ID of the account to set.
 * @param {string} text - The text to set the tooltip to.
 */
function setEditAccountColorTooltip(id, text) {
    var tt = gebi('edit-account-' + id + '-color-tooltip');
    tt.style['display'] = null;
    tt.textContent = text;
}

/**
 * Clear the tooltip for an Edit Account color field.
 *
 * @param {string} id - The ID of the account to clear.
 */
function clearEditAccountColorTooltip(id) {
    var tt = gebi('edit-account-' + id + '-color-tooltip');
    tt.style['display'] = 'none';
    tt.textContent = '';
}

/**
 * Show the color picker when the icon is clicked
 *
 * @param {Event} event - The click event from the icon.
 */
function showEditAccountColorPicker(event) {
    // edit-account-{id}-color-icon
    var id = event.currentTarget.id.slice(13, -11);
    gebi('edit-account-' + id + '-hidden-color-input').click();
}

/**
 * Set the visible Edit Account color field when the hidden input is changed.
 *
 * @param {Event} event - The change event from the hidden input.
 */
function updateEditAccountColor(event) {
    // edit-account-{id}-hidden-color-input
    var id = event.currentTarget.id.slice(13, -19);
    gebi('edit-account-' + id + '-color').value = gebi('edit-account-' + id + '-hidden-color-input').value;
    clearEditAccountColorError(id);
}

/**
 * Set the hidden Edit Account color input when the visible input is changed.
 *
 * @param {Event} event - The change event from the visible input.
 */
function updateEditAccountHiddenColor(event) {
    // edit-account-{id}-color
    var id = event.currentTarget.id.slice(13, -6);
    gebi('edit-account-' + id + '-hidden-color-input').value = gebi('edit-account-' + id + '-color').value;
}

/**
 * Toggle the Edit Account visibility button state.
 *
 * @param {Event} - event - The click event from the button.
 */
function toggleEditAccountVisibilityEvent(event) {
    var id = event.currentTarget.id.slice(13, -9);
    toggleEditAccountVisibility(id);
}

/**
 * Toggle an Edit Account visibility button state.
 *
 * @param {string} id - The ID of the account to toggle.
 */
function toggleEditAccountVisibility(id) {
    var button = gebi('edit-account-' + id + '-hide-btn');
    button.classList.toggle('btn-red-1');
    button.classList.toggle('btn-green-1');
    var icon = gebi('edit-account-' + id + '-hide-btn-icon');
    icon.classList.toggle('icon-visibility');
    icon.classList.toggle('icon-invisible');
    var text = gebi('edit-account-' + id + '-hide-btn-text');
    if (text.textContent === 'Visible') {
        text.textContent = 'Not Visible';
    }
    else {
        text.textContent = 'Visible';
    }
}

/**
 * Save the changes to the account by sending the new data to the server and updating the values with the reply.
 * If there are errors, the fields will be highlighted and tooltip the same as when adding a new account.
 *
 * @param {Event} event - The click event from the save button.
 */
function saveEditAccount(event) {
    // account-{id}-save
    id = event.currentTarget.id.slice(8, -5);
    var name = gebi('edit-account-' + id  + '-name').value;
    var color = gebi('edit-account-' + id  + '-color').value;
    var hide = gebi('edit-account-' + id  + '-hide-btn').classList.contains('btn-red-1') ? 'true' : 'false';

    clearEditAccountErrors(id);

    fetch('/accounts/' + id + '/', {
        method: 'PUT',
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify({'name': name, 'color': color, 'hide': hide})
    })
    .then(response => response.json())
    .then(data => {
        if (data['error']) {
            if (data['type'] === 'general') {
                makeErrorNotification('A general error occurred: ' + data['message'])
            }
            else if (data['type'] === 'parameter') {
                makeErrorNotification(data['message']);
                for (var param in data['data']) {
                    if (param === 'name') {
                        gebi('edit-account-' + id + '-name').classList.add('icon-input-error');
                        setEditAccountNameTooltip(id, data['data'][param].message);
                    }
                    else if (param === 'color') {
                        gebi('edit-account-' + id + '-color').classList.add('icon-input-error');
                        setEditAccountColorTooltip(id, data['data'][param].message);
                    }
                    else if (param === 'hide') {
                        makeErrorNotification(data['data'][param].message + ' (This is likely a bug)');
                    }
                    else {
                        makeErrorNotification('An unknown parameter caused an error, see server logs for details');
                    }
                }
            }
            else {
                makeErrorNotification('An unknown error occurred, see server logs for details');
            }
        }
        else {
            row = gebi('account-' + id);
            row.replaceWith(editingAccounts[id])
            delete editingAccounts[id];
            gebi('account-' + id + '-name').textContent = data['result']['name'];
            var swatch = gebi('account-' + id + '-color-swatch');
            if (data['result']['color'] == null) {
                gebi('account-' + id + '-color').textContent = '';
                swatch.style = null;
                swatch.classList.remove('data-table-color-swatch');
            }
            else {
                gebi('account-' + id + '-color').textContent = '#' + data['result']['color'];
                swatch.style['background-color'] = '#' + data['result']['color'];
                swatch.classList.add('data-table-color-swatch');
            }
            var hideIcon = gebi('account-' + id + '-hide');
            if (data['result']['hide']) {
                hideIcon.classList.add('icon-invisible', 'red-1');
                hideIcon.classList.remove('icon-visibility', 'green-1');
            }
            else {
                hideIcon.classList.remove('icon-invisible', 'red-1');
                hideIcon.classList.add('icon-visibility', 'green-1');
            }
            gebi('account-' + id + '-edit').addEventListener('click', switchToEdit);
        }
    })
    .catch(error => {
        console.error(error);
        makeErrorNotification('There was a communication error, see server logs for details');
    });

}

/**
 * Clear all errors in an Edit Account form.
 *
 * @param {string} id - The ID of the account to clear.
 */
function clearEditAccountErrors(id) {
    clearEditAccountNameError(id);
    clearEditAccountColorError(id);
}

/**
 * Cancel editing an account and return the row to it's original state.
 *
 * @param {Event} event - The click event from the cancel button.
 */
function cancelEditAccount(event) {
    // account-{id}-cancel
    var id = event.currentTarget.id.slice(8, -7);
    row = gebi('account-' + id);
    row.replaceWith(editingAccounts[id])
    delete editingAccounts[id];
    gebi('account-' + id + '-edit').addEventListener('click', switchToEdit);
}
