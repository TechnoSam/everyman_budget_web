from enum import Enum


class ResponseError:
    """Base error, not to be used."""
    def __init__(self):
        self.error_type = 'unknown'


class GeneralError(ResponseError):
    """General error.

    When nothing else will work.
    """
    def __init__(self, message: str):
        super().__init__()
        self.error_type = 'general'
        self.message = message


class ParameterErrorType(Enum):
    """Errors that a client can make."""
    # Nothing else works
    OTHER = 'other'
    # Parameter was required but client did not supply it
    REQUIRED = 'required'
    # Parameter was given with incorrect format
    FORMAT = 'format'
    # The parameter specified a resource that couldn't be found
    NOT_FOUND = 'not_found'
    # The parameter specified that a duplicate resource was to be created and that isn't allowed
    DUPLICATE = 'duplicate'


class ParameterError(ResponseError):
    """Error with client submitted parameters or data"""
    def __init__(self, name: str, param_error_type: ParameterErrorType, message: str):
        super().__init__()
        self.error_type = 'parameter'
        self.name = name
        self.param_error_type = param_error_type
        self.message = message


class ResourceErrorType(Enum):
    OTHER = 'other'
    NOT_FOUND = 'not_found'


class ResourceError(ResponseError):
    def __init__(self, resource_error_type: ResourceErrorType, message: str):
        super().__init__()
        self.error_type = 'resource'
        self.resource_error_type = resource_error_type
        self.message = message


def make_json_response(errors) -> (dict, int):
    """Create a JSON response based on accumulated errors.

    :param errors: List of ResponseErrors.
    :return: (json, code) json: JSON to return with errors or success. Caller will need to fill in data if successful.
             code: HTTP return code derived from errors.
    """
    if len(errors) == 0:
        return {"error": False, "result": {}}, 200

    response = {'error': True}

    for error in errors:
        # This is now pretty brittle. I probably need to scan the whole list first to figure out the level of detail
        # I'm capable of returning
        if type(error) == GeneralError:
            response['type'] = error.error_type
            response['message'] = error.message
            # If we find a GeneralError, we just to terminate early,
            # we have no idea what we can do with any other errors
            if 'data' in response:
                del response['data']
            return response, 500
        elif type(error) == ParameterError:
            response['type'] = error.error_type
            response['message'] = 'One or more parameters are invalid'
            if 'data' not in response:
                response['data'] = {}
            response['data'][error.name] = {'type': error.param_error_type.value, 'message': error.message}
        elif type(error) == ResourceError:
            response['type'] = error.error_type
            response['message'] = error.message
            # Terminate early
            if 'data' in response:
                del response['data']
            return response, 400
        else:
            response['type'] = 'unknown'
            response['message'] = 'An unknown error occurred'
            # If we find an unknown error, we just have to terminate early,
            # we have no idea what we can do with any other errors
            if 'data' in response:
                del response['data']
            return response, 500

    return response, 400
