from datetime import datetime
from decimal import Decimal, ConversionSyntax
import logging
import sys

from flask import Flask, render_template, redirect, request
from werkzeug.datastructures import MultiDict
import colorlog
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from everyman_budget.database import action
from everyman_budget.database.schema import Account, Party, Transaction
from everyman_budget.initialize import is_initialized
import everyman_budget.config as embc

import embweb.config as embwc
from embweb.json_response import *


try:
    embweb_config = embwc.Config()
except embwc.ConfigError as ce:
    print(ce)
    exit(1)

# Configure logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s | %(levelname)-8s | %(message)s | %(name)s:%(lineno)d')
colored_formatter = colorlog.ColoredFormatter(
    '%(asctime)s | %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s | %(name)s:%(lineno)d',
    log_colors={
        'DEBUG': 'bold_cyan',
        'INFO': 'bold_green',
        'WARNING': 'bold_yellow',
        'ERROR': 'bold_red',
        'CRITICAL': 'bold_red,bg_bold_white',
    },
)

if embweb_config.log_to_console:
    stream_handler = logging.StreamHandler()
    stream_handler.setStream(sys.stderr)  # Logging is diagnostic info and should go to stderr
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(colored_formatter if sys.stderr.isatty() else formatter)
    logger.addHandler(stream_handler)

file_handler = logging.FileHandler(embweb_config.log_file)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
logging.getLogger('alembic').setLevel(logging.INFO)
logging.getLogger('werkzeug').setLevel(logging.INFO)
logging.getLogger('everyman_budget.database.action').setLevel(logging.DEBUG)


if not is_initialized():
    print('Database is not initialized')
    exit(1)

emb_config = embc.Config()
engine = create_engine(emb_config.url)
Session = sessionmaker(engine)

app = Flask(__name__)


@app.route('/')
def index_page():
    return redirect('/dashboard/', code=303)


@app.route('/dashboard/')
def dashboard_page():
    return render_template('dashboard.html')


@app.route('/transactions/')
def transactions_page():
    return render_template('transactions.html')


@app.route('/accounts/')
def accounts_page():
    return render_template('accounts.html')


@app.route('/parties/')
def parties_page():
    return render_template('parties.html')


@app.route('/attribution/')
def attribution_page():
    return render_template('attribution.html')


@app.route('/accounts/add/', methods=['POST'])
def add_account():
    """Add a new account"""
    logger.info('Adding new account')
    valid, params, code, json = validate_add_account(request.json)
    if not valid:
        return json, code

    name = params['name']
    hide = params['hide']
    color = params['color']

    logger.debug('Using name={name}'.format(name=name))
    logger.debug('Using hide={hide}'.format(hide=hide))
    logger.debug('Using color={color}'.format(color=color))

    with Session() as session:
        session.begin()

        try:
            account = action.add_account(session, name, hide, color)
        except Exception as e:
            error_text = 'Error adding account: {error}'.format(error=str(e))
            logger.warning(error_text)
            session.rollback()
            json, code = make_json_response([GeneralError('Failed to add account, see logs for details')])
            return json, code

        logger.info('Account added: {account}'.format(account=account))

        try:
            session.commit()
        except Exception as e:
            error_text = 'Error committing add account: {error}'.format(error=str(e))
            logger.warning(error_text)
            session.rollback()
            json, code = make_json_response([GeneralError('Failed to add account, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = account_to_dict(account)
        return json, code


def validate_add_account(data) -> (bool, dict, int, dict):
    """Validate data in a request body to add an account.

    :param data: The new account data from the request body.
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if data is None:
        data = MultiDict()

    params = {}
    errors = []

    name = data.get('name')
    hide = data.get('hide')
    color = data.get('color')

    with Session() as session:
        if parameter_missing(name):
            logger.warning('Parameter "name" is missing')
            errors.append(ParameterError('name', ParameterErrorType.REQUIRED, 'Name is required'))
        else:
            params['name'] = name
            matches = action.find_account_by_name(session, name)
            for match in matches:
                if match.name == name:
                    errors.append(ParameterError('name', ParameterErrorType.DUPLICATE,
                                                 'Account "{name}" already exists'.format(name=name)))
                    del params['name']

        if parameter_missing(hide):
            params['hide'] = False
        else:
            hide, valid, message = validate_bool(hide, 'hide')
            if valid:
                params['hide'] = hide
            else:
                logger.warning(message)
                errors.append(ParameterError('hide', ParameterErrorType.FORMAT, 'Hide must be a boolean'))

        if parameter_missing(color):
            params['color'] = None
        else:
            color, valid, message = validate_color(color)
            if valid:
                params['color'] = color
            else:
                logger.warning(message)
                errors.append(ParameterError('color', ParameterErrorType.FORMAT,
                                             'Color must be a hex code, such as #42ABEF'))

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/accounts/<account_id>/', methods=['GET', 'PUT'])
def access_account(account_id):
    """Get or edit account"""
    if request.method == 'GET':
        logger.info('Getting account with ID {id}'.format(id=account_id))
        with Session() as session:
            account, valid, message = validate_account_id(session, account_id)
            if not valid:
                logger.warning(message)
                return {'message': message}, 400
            logger.debug('Found: {account}'.format(account=account))
            return account_to_dict(account), 200

    logger.info('Editing account with ID {id}'.format(id=account_id))

    valid, params, code, json = validate_put_access_account(account_id, request.json)
    if not valid:
        return json, code

    account = params['account']
    name = params['name']
    hide = params['hide']
    color = params['color']

    logger.debug('Found: {account}'.format(account=account))
    logger.debug('Will edit name to {name}'.format(name=name))
    logger.debug('Will edit hide to {hide}'.format(hide=hide))
    logger.debug('Will edit color to {color}'.format(color=color))

    with Session() as session:
        session.begin()
        # Need to get the account in the same session that it is edited in
        account, valid, message = validate_account_id(session, account_id)
        if not valid:  # Should be impossible, already verified
            logger.error('Failed to get account on edit: {msg}\nSomething has gone horribly wrong'.format(msg=message))
            json, code = make_json_response([GeneralError('Failed to edit account, see logs for details')])
            return json, code
        try:
            action.edit_account(account, name, hide, color)
        except Exception as e:
            session.rollback()
            error_text = 'Error editing account with ID {id}: {error}'.format(id=account_id, error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to edit account, see logs for details')])
            return json, code

        logger.info('Account edited: {account}'.format(account=account))

        try:
            session.commit()
        except Exception as e:
            session.rollback()
            error_text = 'Error committing edit account with ID {id}: {error}'.format(id=account_id, error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to edit account, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = account_to_dict(account)
        return json, code


def validate_put_access_account(account_id, body):
    if body is None:
        body = MultiDict()

    params = {}  # Maybe better name available?
    errors = []

    name = body.get('name')
    hide = body.get('hide')
    color = body.get('color')

    logger.debug('Received name="{name}"'.format(name=name))
    logger.debug('Received hide="{hide}"'.format(hide=hide))
    logger.debug('Received color="{color}"'.format(color=color))

    with Session() as session:
        account, valid, message = validate_account_id(session, account_id)
        if valid:
            params['account'] = account
        else:
            logger.warning(message)
            errors.append(ResourceError(ResourceErrorType.NOT_FOUND, message))

        if parameter_missing(name):
            params['name'] = None
        else:
            params['name'] = name
            matches = action.find_account_by_name(session, name)
            for match in matches:
                if match.name == name and match.id != int(account_id):
                    logger.warning('Account "{name}" already exists (ID {id})'.format(name=name, id=match.id))
                    errors.append(ParameterError('name', ParameterErrorType.DUPLICATE,
                                                 'Account "{name}" already exists'.format(name=name)))
                    del params['name']

        if parameter_missing(hide):
            params['hide'] = None
        else:
            hide, valid, message = validate_bool(hide, 'hide')
            if valid:
                params['hide'] = hide
            else:
                logger.warning(message)
                errors.append(ParameterError('hide', ParameterErrorType.FORMAT, 'Hide must be a boolean'))

        if parameter_missing(color):
            params['color'] = None
        else:
            color, valid, message = validate_color(color)
            if valid:
                params['color'] = color
            else:
                logger.warning(message)
                errors.append(ParameterError('color', ParameterErrorType.FORMAT,
                                             'Color must be a hex code, such as #42ABEF'))

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/accounts/find/', methods=['GET'])
def find_account():
    """Search for an account by name."""
    logger.info('Searching for account')

    search = request.args.get('search')
    if search is None:
        logger.warning('Failed to search for account: search cannot be null')
        return {'message': 'Missing parameter "search"'}, 400
    logger.debug('Search term: {search}'.format(search=search))

    with Session() as session:
        try:
            accounts = action.find_account_by_name(session, search)
        except Exception as e:
            error_text = 'Error searching accounts: {error}'.format(error=str(e))
            logger.error(error_text)
            return {'message': error_text}, 500

        return {'accounts': [
            account_to_dict(account)
            for account in accounts
        ]}, 200


@app.route('/accounts/get/', methods=['GET'])
def get_accounts():
    """Get all accounts."""
    logger.info('Getting all accounts')

    with Session() as session:
        try:
            accounts = action.get_accounts(session)
        except Exception as e:
            error_text = 'Error getting accounts: {error}'.format(error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to get accounts, see server logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = {'accounts': [
            account_to_dict(account)
            for account in accounts
        ]}
        return json, code


@app.route('/accounts/<account_id>/balance/', methods=['GET'])
def get_account_balance(account_id):
    """Get the balance of an account"""
    logger.info('Getting the balance of account with ID {id}'.format(id=account_id))
    date = request.args.get('date')
    if date is not None:
        logger.debug('Will use date={date}'.format(date=date))
        date, valid, message = validate_date(date)
        if not valid:
            logger.warning(message)
            return {'message': message}, 400
        logger.debug('Validated date={date}'.format(date=date))

    order = request.args.get('order')
    if order is not None:
        logger.debug('Will use order={order}'.format(order=order))
        order, valid, message = validate_int(order, 'order')
        if not valid:
            logger.warning(message)
            return {'message': message}, 400
        logger.debug('Validated order={order}'.format(order=order))

    with Session() as session:
        account, valid, message = validate_account_id(session, account_id)
        if not valid:
            logger.warning(message)
            return {'message': message}, 400
        logger.debug('Validated account: {account}'.format(account=account))
        try:
            balance = action.get_account_balance(session, account, date, order)
        except Exception as e:
            error_text = 'Failed to get account balance: {error}'.format(error=str(e))
            logger.error(error_text)
            return {'message': error_text}, 500

        return {
            'account': account_to_dict(account),
            'balance': str(balance),
        }, 200


@app.route('/accounts/balance/', methods=['GET'])
def get_all_account_balances():
    """Get the balance of all accounts"""
    logger.info('Getting the balance of all accounts')

    valid, params, code, json = validate_get_all_accounts_balances(request.args)
    if not valid:
        return json, code

    date = params['date']
    order = params['order']

    logger.debug('Using date={date}'.format(date=date))
    logger.debug('Using order={order}'.format(order=order))

    with Session() as session:
        try:
            accounts = action.get_accounts(session)
        except Exception as e:
            error_text = 'Error getting accounts: {error}'.format(error=str(e))
            logger.error(error_text)
            return {'message': error_text}, 500

        balances = {}
        for account in accounts:
            logger.debug('Getting balance for {account}'.format(account=account))
            try:
                balance = action.get_account_balance(session, account, date, order)
            except Exception as e:
                error_text = 'Failed to get account balance: {error}'.format(error=str(e))
                logger.error(error_text)
                return {'message': error_text}, 500
            balances[str(account.id)] = {
                'account': account_to_dict(account),
                'balance': str(balance),
            }

        json, code = make_json_response([])
        json['result'] = balances
        return json, code


def validate_get_all_accounts_balances(args) -> (bool, dict, int, dict):
    """Validate data in a request query args to add get all account balances.

    :param args: The query parameters for getting account balances.
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if args is None:
        args = MultiDict()

    params = {}
    errors = []

    date = args.get('date')
    order = args.get('order')

    if parameter_missing(date):
        params['date'] = None
    else:
        date, valid, message = validate_date(date)
        if valid:
            params['date'] = date
        else:
            logger.warning(message)
            errors.append(ParameterError('date', ParameterErrorType.FORMAT, 'Date must be in YYYY-MM-DD format'))

    if parameter_missing(order):
        params['order'] = None
    else:
        order, valid, message = validate_int(order, 'order')
        if valid:
            params['order'] = order
        else:
            logger.warning(message)
            errors.append(ParameterError('order', ParameterErrorType.FORMAT, 'Order must be an integer'))

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/parties/add/', methods=['POST'])
def add_party():
    """Add a party"""
    logger.info('Adding party')

    valid, params, code, json = validate_add_party(request.json)
    if not valid:
        return json, code

    name = params['name']

    logger.debug('Using name={name}'.format(name=name))

    with Session() as session:
        session.begin()
        try:
            party = action.add_party(session, name)
        except Exception as e:
            session.rollback()
            error_text = 'Error adding party: {error}'.format(error=str(e))
            logger.warning(error_text)
            json, code = make_json_response([GeneralError('Failed to add party, see logs for details')])
            return json, code

        logger.info('Party added: {party}'.format(party=party))

        try:
            session.commit()
        except Exception as e:
            session.rollback()
            error_text = 'Error committing add party: {error}'.format(error=str(e))
            logger.warning(error_text)
            json, code = make_json_response([GeneralError('Failed to add party, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = party_to_dict(party)
        return json, code


def validate_add_party(data) -> (bool, dict, int, dict):
    """Validate data in a request body to add a party.

    :param data: The new party data from the request body.
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if data is None:
        data = MultiDict()

    params = {}
    errors = []

    name = data['name']

    with Session() as session:
        if parameter_missing(name):
            logger.warning('Parameter "name" is missing')
            errors.append(ParameterError('name', ParameterErrorType.REQUIRED, 'Name is required'))
        else:
            params['name'] = name
            matches = action.find_party_by_name(session, name)
            for match in matches:
                if match.name == name:
                    errors.append(ParameterError('name', ParameterErrorType.DUPLICATE,
                                                 'Party "{name}" already exists'.format(name=name)))
                    del params['name']

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/parties/<party_id>/', methods=['GET', 'PUT'])
def access_party(party_id):
    """Get or edit party"""
    if request.method == 'GET':
        logger.info('Getting party with ID {id}'.format(id=party_id))
        with Session() as session:
            party, valid, message = validate_party_id(session, party_id)
            if not valid:
                logger.warning(message)
                return {'message': message}, 400
            logger.debug('Found: {party}'.format(party=party))
            return party_to_dict(party), 200

    logger.info('Editing party with ID {id}'.format(id=party_id))

    valid, params, code, json = validate_put_access_party(party_id, request.json)
    if not valid:
        return json, code

    party = params['party']
    name = params['name']

    logger.debug('Found: {party}'.format(party=party))
    logger.debug('Will edit name to {name}'.format(name=name))

    with Session() as session:
        session.begin()
        # Need to get the party in the same session that it is edited in
        party, valid, message = validate_party_id(session, party_id)
        if not valid:  # Should be impossible, already verified
            logger.error('Failed to get party on edit: {msg}\nSomething has gone horribly wrong'.format(msg=message))
            json, code = make_json_response([GeneralError('Failed to edit party, see logs for details')])
            return json, code
        try:
            action.edit_party(party, name)
        except Exception as e:
            session.rollback()
            error_text = 'Error editing party with ID {id}: {error}'.format(id=party_id, error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to edit party, see logs for details')])
            return json, code

        logger.info('Edited party: {party}'.format(party=party))

        try:
            session.commit()
        except Exception as e:
            session.rollback()
            error_text = 'Error committing edit party with ID {id}: {error}'.format(id=party_id, error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to edit party, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = party_to_dict(party)
        return json, code


def validate_put_access_party(party_id: str, body: dict) -> (bool, dict, int, dict):
    """Validates a PUT request to access_party, i.e. editing a party.

    :param party_id: The ID of the party to edit.
    :param body: The request body
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if body is None:
        body = MultiDict()

    params = {}
    errors = []

    name = body.get('name')

    logger.debug('Received name="{name}"'.format(name=name))

    with Session() as session:
        party, valid, message = validate_party_id(session, party_id)
        if valid:
            params['party'] = party
        else:
            logger.warning(message)
            errors.append(ResourceError(ResourceErrorType.NOT_FOUND, message))

        if parameter_missing(name):
            params['name'] = None
        else:
            params['name'] = name
            matches = action.find_party_by_name(session, name)
            for match in matches:
                if match.name == name and match.id != int(party_id):
                    logger.warning('Party "{name}" already exists (ID {id})'.format(name=name, id=match.id))
                    errors.append(ParameterError('name', ParameterErrorType.DUPLICATE,
                                                 'Party "{name}" already exists'.format(name=name)))
                    del params['name']

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/parties/find/', methods=['GET'])
def find_party():
    """Search for a party by name"""
    logger.info('Searching for party')

    search = request.args.get('search')
    if search is None:
        logger.warning('Failed to search for party: search cannot be null')
        return {'message': 'Missing parameter "search"'}, 400
    logger.debug('Search term: {search}'.format(search=search))

    with Session() as session:
        try:
            parties = action.find_party_by_name(session, search)
        except Exception as e:
            error_text = 'Error searching parties: {error}'.format(error=str(e))
            logger.error(error_text)
            return {'message': error_text}, 500

        return {'parties': [
            party_to_dict(party)
            for party in parties
        ]}, 200


@app.route('/parties/get/', methods=['GET'])
def get_parties():
    """Get all parties"""
    logger.info('Getting all parties')
    with Session() as session:
        try:
            parties = action.get_parties(session)
        except Exception as e:
            error_text = 'Error getting parties: {error}'.format(error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to get parties, see server logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = {'parties': [
            party_to_dict(party)
            for party in parties
        ]}
        return json, code


@app.route('/transactions/add/', methods=['POST'])
def add_transaction():
    """Add a transaction"""
    logger.info('Adding transaction')

    valid, params, code, json = validate_add_transaction(request.json)
    if not valid:
        return json, code

    date = params['date']
    charge = params['charge']
    credit = params['credit']
    party = params['party']
    account = params['account']
    alt_date = params['alt_date']
    notes = params['notes']

    logger.debug('Using date={date}'.format(date=date))
    logger.debug('Using charge={charge}'.format(charge=charge))
    logger.debug('Using credit={credit}'.format(credit=credit))
    logger.debug('Using party={party}'.format(party=party))
    logger.debug('Using account={account}'.format(account=account))
    logger.debug('Using alt_date={alt_date}'.format(alt_date=alt_date))
    logger.debug('Using notes={notes}'.format(notes=notes))

    with Session() as session:
        session.begin()
        logger.debug('Adding transaction')
        if not credit:
            charge *= -1  # Charge cannot be negative

        try:
            transaction = action.add_transaction(session, account, charge, date,
                                                 alt_date=alt_date, party=party, notes=notes)
        except Exception as e:
            session.rollback()
            error_text = 'Failed to create transaction: {error}'.format(error=str(e))
            logger.warning(error_text)
            json, code = make_json_response([GeneralError('Failed to add transaction, see logs for details')])
            return json, code

        logger.info('New transaction: {tx}'.format(tx=transaction))

        try:
            session.commit()
        except Exception as e:
            session.rollback()
            error_text = 'Failed to commit transaction: {error}'.format(error=str(e))
            logger.warning(error_text)
            json, code = make_json_response([GeneralError('Failed to commit add transaction, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = transaction_to_dict(transaction)
        return json, code


def validate_add_transaction(body) -> (bool, dict, int, dict):
    """Validate data in a request body to add a transaction.

    :param body: The new transaction data from the request body.
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if body is None:
        body = MultiDict()

    params = {}
    errors = []

    date = body.get('date')
    charge = body.get('charge')
    credit = body.get('credit')
    party = body.get('party')
    account = body.get('account')
    alt_date = body.get('alt_date')
    notes = body.get('notes')

    with Session() as session:
        if parameter_missing(date):
            logger.warning('Parameter "date" is missing')
            errors.append(ParameterError('date', ParameterErrorType.REQUIRED, 'Date is required'))
        else:
            date, valid, message = validate_date(date)
            if valid:
                params['date'] = date
            else:
                logger.warning(message)
                errors.append(ParameterError('date', ParameterErrorType.FORMAT, 'Date must be in YYYY-MM-DD format'))

        if parameter_missing(charge):
            logger.warning('Parameter "charge" is missing')
            errors.append(ParameterError('charge', ParameterErrorType.REQUIRED, 'Charge is required'))
        else:
            charge, valid, message = validate_charge(charge)
            if valid:
                if charge < 0:
                    logger.warning('Charge cannot be negative')
                    errors.append(ParameterError('charge', ParameterErrorType.FORMAT, 'Charge cannot be negative'))
                else:
                    params['charge'] = charge
            else:
                logger.warning(message)
                errors.append(ParameterError('charge', ParameterErrorType.FORMAT, 'Charge must be a decimal value'))

        if parameter_missing(credit):
            logger.warning('Parameter "credit" is missing')
            errors.append(ParameterError('credit', ParameterErrorType.REQUIRED, 'Credit is required'))
        else:
            credit, valid, message = validate_bool(credit, 'credit')
            if valid:
                params['credit'] = credit
            else:
                logger.warning(message)
                errors.append(ParameterError('credit', ParameterErrorType.FORMAT, 'Credit must be a boolean'))

        if parameter_missing(party):
            params['party'] = None
        else:
            matches = action.find_party_by_name(session, party)
            found = False
            for match in matches:
                if match.name == party:
                    found = True
                    params['party'] = match
            if not found:
                logger.warning('No party found matching "{name}"'.format(name=party))
                errors.append(ParameterError('party', ParameterErrorType.NOT_FOUND,
                                             'No party found matching "{name}"'.format(name=party)))

        if parameter_missing(account):
            logger.warning('Parameter "account" is missing')
            errors.append(ParameterError('account', ParameterErrorType.REQUIRED, 'Account is required'))
        else:
            matches = action.find_account_by_name(session, account)
            found = False
            for match in matches:
                if match.name == account:
                    found = True
                    params['account'] = match
            if not found:
                logger.warning('No account found matching "{name}"'.format(name=account))
                errors.append(ParameterError('account', ParameterErrorType.NOT_FOUND,
                                             'No account found matching "{name}"'.format(name=account)))

        if parameter_missing(alt_date):
            params['alt_date'] = None
        else:
            alt_date, valid, message = validate_date(alt_date)
            if valid:
                params['alt_date'] = alt_date
            else:
                logger.warning(message)
                errors.append(ParameterError('alt_date', ParameterErrorType.FORMAT,
                                             'Date must be in YYYY-MM-DD format'))

        if parameter_missing(notes):
            params['notes'] = None
        else:
            params['notes'] = notes

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/transactions/add/transfer/', methods=['POST'])
def transfer():
    """Transfer an amount between two accounts"""
    # TODO Make sure notes go on the txs if I can't see the group
    logger.info('Adding transfer')

    valid, params, code, json = validate_add_transfer(request.json)
    if not valid:
        return json, code

    date = params['date']
    charge = params['charge']
    from_account = params['from_account']
    to_account = params['to_account']
    notes = params['notes']

    logger.debug('Using date={date}'.format(date=date))
    logger.debug('Using charge={charge}'.format(charge=charge))
    logger.debug('Using from_account={from_account}'.format(from_account=from_account))
    logger.debug('Using to_account={to_account}'.format(to_account=to_account))
    logger.debug('Using notes={notes}'.format(notes=notes))

    with Session() as session:
        session.begin()
        try:
            action.transfer(session, to_account, from_account, charge, date, notes)
        except Exception as e:
            session.rollback()
            error_text = 'Error adding transfer: {error}'.format(error=str(e))
            logger.warning(error_text)
            json, code = make_json_response([GeneralError('Failed to add transfer, see logs for details')])
            return json, code

        logger.debug('Transfer completed')

        try:
            session.commit()
        except Exception as e:
            session.rollback()
            error_text = 'Error committing add transfer: {error}'.format(error=str(e))
            logger.warning(error_text)
            json, code = make_json_response([GeneralError('Failed to commit add transfer, see logs for details')])
            return json, code

        json, code = make_json_response([])
        return json, code


def validate_add_transfer(body) -> (bool, dict, int, dict):
    """Validate data in a request body to add a transfer.

    :param body: The new transfer data from the request body.
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if body is None:
        body = MultiDict()

    params = {}
    errors = []

    to_account = body.get('to_account')
    from_account = body.get('from_account')
    charge = body.get('charge')
    date = body.get('date')
    notes = body.get('notes')

    with Session() as session:
        if parameter_missing(date):
            logger.warning('Parameter "date" is missing')
            errors.append(ParameterError('date', ParameterErrorType.REQUIRED, 'Date is required'))
        else:
            date, valid, message = validate_date(date)
            if valid:
                params['date'] = date
            else:
                logger.warning(message)
                errors.append(ParameterError('date', ParameterErrorType.FORMAT, 'Date must be in YYYY-MM-DD format'))

        if parameter_missing(charge):
            logger.warning('Parameter "charge" is missing')
            errors.append(ParameterError('charge', ParameterErrorType.REQUIRED, 'Charge is required'))
        else:
            charge, valid, message = validate_charge(charge)
            if valid:
                if charge < 0:
                    logger.warning('Charge cannot be negative')
                    errors.append(ParameterError('charge', ParameterErrorType.FORMAT, 'Charge cannot be negative'))
                else:
                    params['charge'] = charge
            else:
                logger.warning(message)
                errors.append(ParameterError('charge', ParameterErrorType.FORMAT, 'Charge must be a decimal value'))

        if parameter_missing(from_account):
            logger.warning('Parameter "from_account" is missing')
            errors.append(ParameterError('from_account', ParameterErrorType.REQUIRED, 'From Account is required'))
        else:
            matches = action.find_account_by_name(session, from_account)
            found = False
            for match in matches:
                if match.name == from_account:
                    found = True
                    params['from_account'] = match
            if not found:
                logger.warning('No account found matching "{name}"'.format(name=from_account))
                errors.append(ParameterError('from_account', ParameterErrorType.NOT_FOUND,
                                             'No account found matching "{name}"'.format(name=from_account)))

        if parameter_missing(to_account):
            logger.warning('Parameter "to_account" is missing')
            errors.append(ParameterError('to_account', ParameterErrorType.REQUIRED, 'To Account is required'))
        else:
            matches = action.find_account_by_name(session, to_account)
            found = False
            for match in matches:
                if match.name == to_account:
                    found = True
                    params['to_account'] = match
            if not found:
                logger.warning('No account found matching "{name}"'.format(name=to_account))
                errors.append(ParameterError('to_account', ParameterErrorType.NOT_FOUND,
                                             'No account found matching "{name}"'.format(name=to_account)))

        if parameter_missing(notes):
            params['notes'] = None
        else:
            params['notes'] = notes

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/transactions/<transaction_id>/', methods=['GET', 'PUT', 'DELETE'])
def access_transaction(transaction_id):
    """Get or edit a transaction."""
    if request.method == 'GET':
        logger.info('Getting transaction with ID {id}'.format(id=transaction_id))
        with Session() as session:
            transaction, valid, message = validate_transaction_id(session, transaction_id)
            if not valid:
                logger.warning(message)
                return {'message': message}, 400
            logger.debug('Found: {tx}'.format(tx=transaction))
            return transaction_to_dict(transaction), 200

    if request.method == 'DELETE':
        logger.info('Deleting transaction with ID {id}'.format(id=transaction_id))
        with Session() as session:
            session.begin()
            transaction, valid, message = validate_transaction_id(session, transaction_id)
            if not valid:
                logger.warning(message)
                json, code = make_json_response([ResourceError(ResourceErrorType.NOT_FOUND, message)])
                return json, code

            try:
                action.delete_transaction(session, transaction)
            except Exception as e:
                session.rollback()
                error_text = 'Failed to delete transaction with ID {id}: {error}'.format(
                    id=transaction_id, error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error deleting transaction')])
                return json, code

            logger.info('Transaction deleted')

            try:
                session.commit()
            except Exception as e:
                session.rollback()
                error_text = 'Failed to commit delete transaction with ID {id}: {error}'.format(
                    id=transaction_id, error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error committing delete transaction')])
                return json, code

            json, code = make_json_response([])
            return json, code

    logger.info('Editing transaction with ID {id}'.format(id=transaction_id))

    valid, params, code, json = validate_put_access_transaction(transaction_id, request.json)
    if not valid:
        return json, code

    transaction = params['transaction']
    date = params['date']
    order = params['order']
    charge = params['charge']
    party = params['party']
    account = params['account']
    alt_date = params['alt_date']
    notes = params['notes']

    logger.debug('Using tx={tx}'.format(tx=transaction))
    logger.debug('Using date={date}'.format(date=date))
    logger.debug('Using order={order}'.format(order=order))
    logger.debug('Using charge={charge}'.format(charge=charge))
    logger.debug('Using party={party}'.format(party=party))
    logger.debug('Using account={account}'.format(account=account))
    logger.debug('Using alt_date={alt_date}'.format(alt_date=alt_date))
    logger.debug('Using notes={notes}'.format(notes=notes))

    with Session() as session:
        session.begin()
        transaction = action.get_transaction_by_id(session, transaction.id)  # Get tx in this session so we can edit it

        logger.debug('Editing transaction: {tx}'.format(tx=transaction))

        do_edit_account = False
        do_edit_charge = False
        do_move = False
        do_edit_party = False
        do_edit_notes = False
        do_edit_alt_date = False

        if account is not None:
            if transaction.account.id == account.id:
                logger.debug('Skipping edit account because it is not different')
            else:
                logger.debug('Will edit account to {account}'.format(account=account))
                do_edit_account = True

        if charge is not None:
            if transaction.charge == charge:
                logger.debug('Skipping edit charge because it is not different')
            else:
                logger.debug('Will edit charge to {charge}'.format(charge=charge))
                do_edit_charge = True

        if date is not None:
            if order is not None:
                if transaction.date == date.date() and transaction.order == order:
                    logger.debug('Skipping move because date and order are not different')
                else:
                    logger.debug('Will move to {date}, order {order}'.format(date=date, order=order))
                    do_move = True
            else:
                if transaction.date == date.date():
                    logger.debug('Skipping move because date is not different')
                else:
                    logger.debug('Will move to {date}'.format(date=date))
                    do_move = True

        if party is not None:
            if transaction.party.id == party.id:
                logger.debug('Skipping edit party because it is not different')
            else:
                logger.debug('Will edit party to {party}'.format(party=party))
                do_edit_party = True

        if alt_date is not None:
            if transaction.alt_date == alt_date.date():
                logger.debug('Skipping edit alt_date because it is not different')
            else:
                logger.debug('Will edit alt_date to {date}'.format(date=alt_date))
                do_edit_alt_date = True

        if notes is not None:
            if transaction.notes == notes:
                logger.debug('Skipping edit notes because it is not different')
            else:
                logger.debug('Will edit notes to {notes}'.format(notes=notes))
                do_edit_notes = True

        if do_edit_account:
            logger.debug('Editing account to {account}'.format(account=account))
            try:
                action.edit_transaction_account(session, transaction, account)
            except Exception as e:
                session.rollback()
                error_text = 'Error editing transaction account: {error}'.format(error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error editing transaction, see logs for details')])
                return json, code

        if do_edit_charge:
            logger.debug('Editing charge to {charge}'.format(charge=charge))
            try:
                action.edit_transaction_charge(session, transaction, charge)
            except Exception as e:
                session.rollback()
                error_text = 'Error editing transaction charge: {error}'.format(error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error editing transaction, see logs for details')])
                return json, code

        if do_move:
            logger.debug('Moving transaction to {date}, {order}'.format(date=date, order=order))
            try:
                action.move_transaction(session, transaction, date, order)
            except Exception as e:
                session.rollback()
                error_text = 'Error editing moving transaction: {error}'.format(error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error editing transaction, see logs for details')])
                return json, code

        if do_edit_party:
            logger.debug('Editing party to {party}'.format(party=party))
            try:
                action.edit_transaction_party(transaction, party)
            except Exception as e:
                session.rollback()
                error_text = 'Error editing transaction party: {error}'.format(error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error editing transaction, see logs for details')])
                return json, code

        if do_edit_alt_date:
            logger.debug('Editing alt_date to {date}'.format(date=alt_date))
            try:
                action.edit_transaction_alt_date(transaction, alt_date)
            except Exception as e:
                session.rollback()
                error_text = 'Error editing transaction alt_date: {error}'.format(error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error editing transaction, see logs for details')])
                return json, code

        if do_edit_notes:
            logger.debug('Editing notes to {notes}'.format(notes=notes))
            try:
                action.edit_transaction_notes(transaction, notes)
            except Exception as e:
                session.rollback()
                error_text = 'Error editing transaction notes: {error}'.format(error=str(e))
                logger.error(error_text)
                json, code = make_json_response([GeneralError('Error editing transaction, see logs for details')])
                return json, code

        logger.info('Transaction edited: {tx}'.format(tx=transaction))

        try:
            session.commit()
        except Exception as e:
            session.rollback()
            error_text = 'Error committing edit transaction: {error}'.format(error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Error committing edit transaction, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = transaction_to_dict(transaction)
        return json, code


def validate_put_access_transaction(transaction_id: str, body: dict) -> (bool, dict, int, dict):
    """Validates a PUT request to access_transaction, i.e. editing a transaction.

    :param transaction_id: The ID of the transaction to edit.
    :param body: The request body
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if body is None:
        body = MultiDict()

    params = {}
    errors = []

    date = body.get('date')
    order = body.get('order')
    charge = body.get('charge')
    party = body.get('party')
    account = body.get('account')
    alt_date = body.get('alt_date')
    notes = body.get('notes')

    with Session() as session:
        tx, valid, message = validate_transaction_id(session, transaction_id)
        if valid:
            params['transaction'] = tx
        else:
            logger.warning(message)
            errors.append(ResourceError(ResourceErrorType.NOT_FOUND, message))

        if parameter_missing(date):
            params['date'] = None
        else:
            date, valid, message = validate_date(date)
            if valid:
                params['date'] = date
            else:
                logger.warning(message)
                errors.append(
                    ParameterError('date', ParameterErrorType.FORMAT, 'Date must be in YYYY-MM-DD format'))

        if parameter_missing(order):
            params['order'] = None
        else:
            order, valid, message = validate_int(order, 'order')
            if valid:
                params['order'] = order
            else:
                logger.warning(message)
                errors.append(ParameterError('order', ParameterErrorType.FORMAT, 'Order must be an integer'))

        if parameter_missing(charge):
            params['charge'] = None
        else:
            charge, valid, message = validate_charge(charge)
            if valid:
                params['charge'] = charge
            else:
                logger.warning(message)
                errors.append(ParameterError('charge', ParameterErrorType.FORMAT, 'Charge must be a decimal value'))

        if parameter_missing(party):
            params['party'] = None
        else:
            matches = action.find_party_by_name(session, party)
            found = False
            for match in matches:
                if match.name == party:
                    found = True
                    params['party'] = match
            if not found:
                logger.warning('No party found matching "{name}"'.format(name=party))
                errors.append(ParameterError('party', ParameterErrorType.NOT_FOUND,
                                             'No party found matching "{name}"'.format(name=party)))

        if parameter_missing(account):
            params['account'] = None
        else:
            matches = action.find_account_by_name(session, account)
            found = False
            for match in matches:
                if match.name == account:
                    found = True
                    params['account'] = match
            if not found:
                logger.warning('No account found matching "{name}"'.format(name=account))
                errors.append(ParameterError('account', ParameterErrorType.NOT_FOUND,
                                             'No account found matching "{name}"'.format(name=account)))

        if parameter_missing(alt_date):
            params['alt_date'] = None
        else:
            alt_date, valid, message = validate_date(alt_date)
            if valid:
                params['alt_date'] = alt_date
            else:
                logger.warning(message)
                errors.append(ParameterError('alt_date', ParameterErrorType.FORMAT,
                                             'Date must be in YYYY-MM-DD format'))

        if parameter_missing(notes):
            params['notes'] = None
        else:
            params['notes'] = notes

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


@app.route('/transactions/get/', methods=['GET'])
def get_transactions():
    """Get transactions with a filter."""
    logger.info('Getting transactions')

    valid, params, code, json = validate_get_transactions(request.args)
    if not valid:
        return json, code

    accounts = params['accounts']
    charge_begin = params['charge_begin']
    charge_end = params['charge_end']
    parties = params['parties']
    date_begin = params['date_begin']
    date_end = params['date_end']
    debit = params['debit']
    notes_contain = params['notes_contain']

    logger.debug('Will filter with accounts: {accounts}'.format(accounts=accounts))
    logger.debug('Will filter with charge_begin={charge}'.format(charge=charge_begin))
    logger.debug('Will filter with charge_end={charge}'.format(charge=charge_end))
    logger.debug('Will filter with parties: {parties}'.format(parties=parties))
    logger.debug('Will filter with date_begin={date}'.format(date=date_begin))
    logger.debug('Will filter with date_end={date}'.format(date=date_end))
    logger.debug('Will filter with debit={debit}'.format(debit=debit))
    logger.debug('Will filter with notes_contain={notes}'.format(notes=notes_contain))

    with Session() as session:
        try:
            transactions = action.get_transactions(session, accounts,
                                                   charge_begin, charge_end,
                                                   parties, date_begin, date_end,
                                                   debit, notes_contain)
        except Exception as e:
            error_text = 'Error getting transactions: {error}'.format(error=str(e))
            logger.error(error_text)
            json, code = make_json_response([GeneralError('Failed to get transactions, see logs for details')])
            return json, code

        json, code = make_json_response([])
        json['result'] = [
                transaction_to_dict(transaction)
                for transaction in transactions
            ]
        return json, code


def validate_get_transactions(args) -> (bool, dict, int, dict):
    """Validate data in a request query args to get transactions

    :param args: The request query args.
    :return: (valid, params, code, json) valid: if the data is valid, params: dict of params if valid,
             code: HTTP return code, json: dict to be returned to client. If no error,
             caller should set any return data before sending to the client.
    """
    if args is None:
        args = MultiDict()

    params = {}
    errors = []

    accounts = args.get('accounts')
    charge_begin = args.get('charge_begin')
    charge_end = args.get('charge_end')
    parties = args.get('parties')
    date_begin = args.get('date_begin')
    date_end = args.get('date_end')
    debit = args.get('debit')
    notes_contain = args.get('notes_contain')

    with Session() as session:
        if parameter_missing(accounts):
            params['accounts'] = None
        else:
            account_names = [x.strip() for x in accounts.split(',')]
            accounts = []
            names_not_found = []
            for name in account_names:
                matches = action.find_account_by_name(session, name)
                found = False
                for match in matches:
                    if match.name == name:
                        found = True
                        accounts.append(match)
                if not found:
                    logger.warning('No account found matching {name}'.format(name=name))
                    names_not_found.append(name)

            if len(names_not_found) > 0:
                errors.append(ParameterError('accounts', ParameterErrorType.NOT_FOUND,
                                             'The following accounts were not found: {names}'.format(
                                                 names=', '.join(names_not_found))))
            else:
                params['accounts'] = accounts

        if parameter_missing(charge_begin):
            params['charge_begin'] = None
        else:
            charge_begin, valid, message = validate_charge(charge_begin)
            if valid:
                params['charge_begin'] = charge_begin
            else:
                logger.warning('Charge Begin is not valid: {msg}'.format(msg=message))
                errors.append(ParameterError('charge_begin', ParameterErrorType.FORMAT,
                                             'Charge must be a decimal value'))

        if parameter_missing(charge_end):
            params['charge_end'] = None
        else:
            charge_end, valid, message = validate_charge(charge_end)
            if valid:
                params['charge_end'] = charge_end
            else:
                logger.warning('Charge End is not valid: {msg}'.format(msg=message))
                errors.append(ParameterError('charge_end', ParameterErrorType.FORMAT,
                                             'Charge must be a decimal value'))

        if parameter_missing(parties):
            params['parties'] = None
        else:
            party_names = [x.strip() for x in parties.split(',')]
            parties = []
            names_not_found = []
            for name in party_names:
                matches = action.find_party_by_name(session, name)
                found = False
                for match in matches:
                    if match.name == name:
                        found = True
                        parties.append(match)
                if not found:
                    logger.warning('No party found matching {name}'.format(name=name))
                    names_not_found.append(name)

            if len(names_not_found) > 0:
                errors.append(ParameterError('parties', ParameterErrorType.NOT_FOUND,
                                             'The following parties were not found: {names}'.format(
                                                 names=','.join(names_not_found))))
            else:
                params['parties'] = parties

        if parameter_missing(date_begin):
            params['date_begin'] = None
        else:
            date_begin, valid, message = validate_date(date_begin)
            if valid:
                params['date_begin'] = date_begin
            else:
                logger.warning('Date Begin is not valid: {msg}'.format(msg=message))
                errors.append(ParameterError('date_begin', ParameterErrorType.FORMAT,
                                             'Date must be in the YYYY-MM-DD format'))

        if parameter_missing(date_end):
            params['date_end'] = None
        else:
            date_end, valid, message = validate_date(date_end)
            if valid:
                params['date_end'] = date_end
            else:
                logger.warning('Date End is not valid: {msg}'.format(msg=message))
                errors.append(ParameterError('date_end', ParameterErrorType.FORMAT,
                                             'Date must be in the YYYY-MM-DD format'))

        if parameter_missing(debit):
            params['debit'] = None
        else:
            debit, valid, message = validate_bool(debit, 'debit')
            if valid:
                params['debit'] = debit
            else:
                logger.warning(message)
                errors.append(ParameterError('debit', ParameterErrorType.FORMAT, 'Debit must be a bool'))

        if parameter_missing(notes_contain):
            params['notes_contain'] = None
        else:
            params['notes_contain'] = notes_contain

    json, code = make_json_response(errors)

    return not json['error'], params, code, json


def account_to_dict(account: Account):
    """Return the dictionary representation of an account.

    :param account: The account to represent.
    :return: The dictionary representation of account.
    """
    if account is None:
        return None
    return {
        'id': account.id,
        'name': account.name,
        'hide': account.hide,
        'color': account.color,
    }


def party_to_dict(party: Party):
    """Return the dictionary representation of a party.

    :param party: The party to represent.
    :return: The dictionary representation of party.
    """
    if party is None:
        return None
    return {
        'id': party.id,
        'name': party.name,
    }


def transaction_to_dict(transaction: Transaction):
    """Return the dictionary representation of a transaction.

    :param transaction: The transaction to represent.
    :return: The dictionary representation of transaction.
    """
    if transaction is None:
        return None
    return {
        'id': transaction.id,
        'account': account_to_dict(transaction.account),
        'charge': str(transaction.charge),
        'party': party_to_dict(transaction.party),
        'date': transaction.date.strftime('%Y-%m-%d'),
        'alt_date': transaction.alt_date.strftime('%Y-%m-%d') if transaction.alt_date is not None else None,
        'order': transaction.order,
        'notes': transaction.notes,
    }


def parameter_missing(parameter: str) -> bool:
    """Check if a parameter should be considered missing.

    :return: True if a parameter should be considered missing. (It is null, an empty string, or only whitespace)
    """
    return parameter is None or len(parameter) == 0 or parameter.isspace()


def validate_int(parameter: str, name: str) -> (int, bool, str):
    """Validates an integer.

    :param parameter: The value to validate.
    :param name: The name of this parameter, used for generating error message
    :return: value: The converted value (None if invalid), valid: whether or not the value is valid,
             message: error message if invalid.
    """
    try:
        parameter = int(parameter)
    except ValueError:
        return None, False, 'Parameter {name} is not a valid integer'.format(name=name)
    return parameter, True, ''


def validate_bool(parameter: str, name: str) -> (bool, bool, str):
    """Validates a bool.

    :param parameter: The value to validate.
    :param name: The name of this parameter, used for generating error message
    :return: value: The converted value (None if invalid), valid: whether or not the value is valid,
             message: error message if invalid.
    """
    parameter = parameter.lower()
    if parameter in ['true', '1']:
        return True, True, ''
    if parameter in ['false', '0']:
        return False, True, ''
    return None, False, 'Parameter {name} is not a valid boolean'.format(name=name)


def validate_date(date: str) -> (datetime, bool, str):
    """Validates a date.

    :param date: The date string to validate.
    :return: date: The converted datetime (None if invalid), valid: whether or not the value is valid,
             message: error message if invalid.
    """
    try:
        date = datetime.strptime(date, "%Y-%m-%d")
    except ValueError as e:
        return None, False, 'Invalid date: {error}'.format(error=str(e))
    except Exception as e:
        return None, False, 'Invalid date, unknown error: {error}'.format(error=str(e))
    return date, True, ''


def validate_color(color: str) -> (str, bool, str):
    """Validate a color.

    :param color: The value to validate.
    :return: (color, valid, message) color: The converted color (None if invalid). valid: True if the value is valid.
             message: Error message if invalid.
    """
    color = color.lower()
    if color[0] == '#':
        color = color[1:]
    if len(color) != 6:
        return None, False, 'Invalid color, must be exactly six hex digits'
    for char in color:
        if char not in '0123456789abcdef':
            return None, False, 'Invalid color, must be exactly six hex digits'
    return color, True, ''


def validate_charge(charge: str) -> (Decimal, bool, str):
    """Validates a Decimal.

    :param charge: The value to validate.
    :return: charge: The converted Decimal (None if invalid), valid: whether or not the value is valid,
             message: error message if invalid.
    """
    try:
        charge = Decimal(charge)
    except ConversionSyntax:
        return None, False, 'Invalid charge format: {charge}'.format(charge=charge)
    except Exception as e:
        return None, False, 'Charge unknown error: {error}'.format(error=str(e))

    # TODO Can the database do this validation for me?
    # If the denominator of the ratio is not 1 after 4 left shifts, the user has given us something with more than
    # four decimal places and is invalid.
    if charge.shift(4).as_integer_ratio()[1] != 1:
        return None, False, 'Invalid charge format: The database can only store numbers with a scale of 4'
    # If the value after 15 right sifts is more than or equal to 1, we have more than 15 digits in the integer part
    if charge.shift(-15) >= 1:
        return None, False, 'Invalid charge format: The database can only store numbers with' \
                            ' a precision of 19 and a scale of 4 (max 15 digits of integer part)'
    return charge, True, ''


def validate_account_id(session: Session, account_id: str) -> (Account, bool, str):
    """Validates an account ID.

    :param session: The database session.
    :param account_id: The ID to validate
    :return: account: The account from the database, valid: whether or not the ID is valid,
             message: error message if invalid.
    """
    account_id, valid, message = validate_int(account_id, 'account_id')
    if not valid:
        return None, False, message
    try:
        account = action.get_account_by_id(session, account_id)
    except Exception:
        return None, False, 'No account with ID {id}'.format(id=account_id)
    return account, True, ''


def validate_party_id(session: Session, party_id: str) -> (Party, bool, str):
    """Validates a party ID.

    :param session: The database session.
    :param party_id: The ID to validate
    :return: party: The party from the database, valid: whether or not the ID is valid,
             message: error message if invalid.
    """
    party_id, valid, message = validate_int(party_id, 'party_id')
    if not valid:
        return None, False, message
    try:
        party = action.get_party_by_id(session, party_id)
    except Exception:
        return None, False, 'No party with ID {id}'.format(id=party_id)
    return party, True, ''


def validate_transaction_id(session: Session, transaction_id: str) -> (Transaction, bool, str):
    """Validates a transaction ID.

    :param session: The database session.
    :param transaction_id: The ID to validate
    :return: transaction: The transaction from the database, valid: whether or not the ID is valid,
             message: error message if invalid.
    """
    transaction_id, valid, message = validate_int(transaction_id, 'transaction_id')
    if not valid:
        return None, False, message
    try:
        transaction = action.get_transaction_by_id(session, transaction_id)
    except Exception:
        return None, False, 'No transaction with ID {id}'.format(id=transaction_id)
    return transaction, True, ''
