import os
import logging

import toml

CONFIG_PATH_VAR_NAME = 'EMBW_CONFIG'

logger = logging.getLogger(__name__)


class ConfigError(RuntimeError):
    pass


class Config:
    def __init__(self):
        if CONFIG_PATH_VAR_NAME not in os.environ:
            logger.error('Environment variable {var} is not set'.format(var=CONFIG_PATH_VAR_NAME))
            raise ConfigError('Environment variable {var} is not set. '
                              'Refer to the documentation to read about required environment variables.'
                              .format(var=CONFIG_PATH_VAR_NAME))
        config_path = os.environ[CONFIG_PATH_VAR_NAME]
        # Read config file. For now, mostly just need logging config

        try:
            config_toml = toml.load(config_path)
        except toml.TomlDecodeError as e:
            logger.error('Failed to load config file: {message}'.format(message=str(e)))
            raise ConfigError('Failed to load config file: {message}'.format(message=str(e)))

        self.log_to_console = config_toml['logging']['console']
        self.log_file = config_toml['logging']['file']
