# EMBW Docker

## Prerequisites

Install [Docker Desktop](https://www.docker.com/products/docker-desktop). (Or install just the
[Engine](https://docs.docker.com/engine/install/) on Linux)

If you are on Windows 10 Home Edition, you will need to follow some additional steps,
[documented here](https://docs.docker.com/desktop/windows/install/). It's nothing major, you just need to install WSL2
and enable virtualization in your BIOS.

## Configuration

The default configuration provided by `docker-compose.yml` should probably be good as is for almost everyone except for
the database password. You should change this to something securely generated. Make sure to change it both times that it
appears, both containers need to know the password. Note that this should be done before the first run.

If you just want to get the service running, you can skip the rest of this section.

If you want to stop the logs from filling up your disk, you can remove the logs line from the volumes. To save space,
you can also just delete the logs from your disk. The files will be recreated without errors for subsequent logging.

You can change the port in the `ports` section.

If you don't want the service to automatically restart, you can set `restart` to `no`, or `unless-stopped`.

If you feel the need to change anything else, I assume you're smart enough to figure out what needs to be done.

The EMBW App image is not on docker hub, but compose will build it when it needs it.

## Launching

Make sure that Docker is running.

```bash
cd docker/embw
docker-compose up -d
```

Assuming you haven't changed the port, the service will be available at
[http://localhost:45399](http://localhost:45399).

The services are configured to restart automatically, even if you restart the system.
To stop the services, use `docker-compose down` from the same
directory.

## Backup / Restore

Backup and Restore scripts are provided for your convenience. PowerShell (ps1) for Windows, Bash (sh) for others.
The backup dumps the database and archives it with the configuration. You should run the backup periodically,
on an automatic job if possible.

If you are on Windows, you may need to change your
[Execution Policy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-7.1)
or run an Administrator PowerShell to run the scripts because they are unsigned.

The restore script needs to be given the path to the `database.dump` file.

```bash
./restore.sh <path_to_dump>
```

Note that restoring a backup will completely erase all the existing data in the database and replace it with whatever is
in the backup.

You can restore using a backup made on any system. Meaning you can make the backup on Windows but restore it on Linux.
