#!/bin/bash

SERVICE="emb"
SOURCE_DIR=.
DESTINATION_DIR=.

DB_USERNAME="embw"
DB_NAME="embw_production"

DATE=`date +"%Y_%m_%d.%H.%M.%S"`
echo "Creating ${SERVICE} backup at $DATE"

echo "Changing to ${SOURCE_DIR}"
pushd $SOURCE_DIR

# Dump PostgreSQL database
echo "Dumping database"
docker-compose exec -T db pg_dump --username $DB_USERNAME --format custom $DB_NAME > database.dump
exit_code=$?
if [ $exit_code -ne 0 ]; then
    rm database.dump
    echo "Error. Is the docker container running?"
    exit 1
fi

# Archive
ARCHIVE="${SERVICE}_backup_${DATE}.tar.gz"
ARCHIVE_PATH="${DESTINATION_DIR}/${ARCHIVE}"
echo "Creating $ARCHIVE_PATH"

tar -czf $ARCHIVE_PATH "docker-compose.yml" "database.dump"

echo "Cleaning up"
# Delete database dump
rm database.dump
