#!/bin/bash

cd everyman_budget_web

# Try to initialize the database in case this is the first run
python3 everyman_budget/everyman_budget/initialize.py --log-file /home/embw/logs/init.log --log-console

# Try to migrate the data in case this is the first run after an upgrade
cd everyman_budget
python3 -m alembic upgrade
cd ..

# Start the server
python3 -m flask run --host=0.0.0.0
