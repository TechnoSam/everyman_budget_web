$SERVICE = "emb"
$SOURCE_DIR = "."
$DESTINATION_DIR = "."

$DB_USERNAME = "embw"
$DB_NAME = "embw_production"

$DATE = Get-Date -Format "yyyy_MM_dd.HH.mm.ss"
Write-Output "Creating $SERVICE backup at $DATE"

Write-Output "Changing to $SOURCE_DIR"
Push-Location $SOURCE_DIR

# Dump PostgreSQL database
Write-Output "Dumping database"
$backup_proc = Start-Process docker-compose -ArgumentList "exec -T db pg_dump --username $DB_USERNAME --format custom $DB_NAME" -RedirectStandardOutput database.dump -NoNewWindow -Wait -PassThru
if ($backup_proc.exitCode -ne 0) {
    Remove-Item -Force database.dump
    Write-Output "Error. Is the docker container running?"
    exit 1
}

# Archive
$ARCHIVE = "${SERVICE}_backup_${DATE}.zip"
$ARCHIVE_PATH = "${DESTINATION_DIR}/${ARCHIVE}"
Write-Output "Creating $ARCHIVE_PATH"

Compress-Archive -Path "docker-compose.yml", "database.dump" -DestinationPath $ARCHIVE_PATH

Write-Output "Cleaning up"
# Delete database dump
Remove-Item -Force database.dump
