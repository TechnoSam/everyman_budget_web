#!/bin/bash

DB_USERNAME="embw"
DB_NAME="embw_production"

# Usage
if [[ $# -lt 1 ]]; then
    echo "usage: restore.sh <path_to_dump>"
    exit 1
fi

# First argument is the path to the dump file to restore
DUMP_PATH=$1

echo "RESTORING WILL DELETE YOUR EXISTING DATA IN THE DATABASE"
echo "ARE YOU SURE YOU WANT TO CONTINUE?"
echo -n "ENTER \"DELETE\" TO CONTINUE: "
read confirm

if [[ "$confirm" != "DELETE" ]]; then
    echo "Aborting"
    exit 0
fi

echo "Restoring from $DUMP_PATH"
docker-compose exec -T db pg_restore --clean --username $DB_USERNAME --dbname $DB_NAME < $DUMP_PATH
exit_code=$?
if [ $exit_code -ne 0 ]; then
    echo "Error. Is the docker container running? Is your path to the dump correct?"
    exit 1
fi

echo "Completed restore"
