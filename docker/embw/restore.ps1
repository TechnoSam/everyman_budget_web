$DB_USERNAME = "embw"
$DB_NAME = "embw_production"

# Usage
if ($args.count -ne 1) {
    Write-Output "usage: restore.ps1 <path_to_dump>"
    exit 1
}

# First argument is the path to the dump file to restore
$DUMP_PATH = $args[0]

Write-Output "RESTORING WILL DELETE YOUR EXISTING DATA IN THE DATABASE"
Write-Output "ARE YOU SURE YOU WANT TO CONTINUE?"
$confirm = Read-Host 'ENTER "DELETE" TO CONTINUE'

if (!$confirm.equals("DELETE")) {
    Write-Output "Aborting"
    exit 0
}

Write-Output "Restoring from $DUMP_PATH"
$restore_proc = Start-Process docker-compose -ArgumentList "exec -T db pg_restore --clean --username $DB_USERNAME --dbname $DB_NAME" -RedirectStandardInput $DUMP_PATH -NoNewWindow -Wait -PassThru
if ($restore_proc.exitCode -ne 0) {
    Write-Output "Error. Is the docker container running? Is your path to the dump correct?"
    exit 1
}

Write-Output "Completed restore"
